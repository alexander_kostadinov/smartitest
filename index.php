<?php
include 'init.php';
if (isset($_SESSION['username'])) {
    header('Location: main.php');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html prefix="og:http://ogp.me/ns#" ng-app="smarti" lang="en"> 
    <head>
        <meta property="og:image" content="http://sandroid.info/thesis/Assets/Images/welcome.jpg">
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Smarti Test System</title>
        <link rel="icon" type="image/png" href="Assets/Images/favicon.png">
        <script src="Assets/Library/Angular/angular.js"></script>
        <script src="Assets/Library/Angular/angular-route.js"></script>
        <script src="Assets/Library/Angular/angular-animate.js"></script>
        <script src="Assets/Library/Angular/angular-cookies.js"></script>
        <script src="Assets/Library/Angular/angular-resource.js"></script>
        <script src="Assets/Library/Angular/angular-sanitize.js"></script>
        <script src="Assets/Library/jQuery/jquery-1.11.0.js"></script>
        <script src="Assets/Library/Bootstrap/js/bootstrap.js"></script>
        <script src="Assets/Library/Bootstrap/js/bootbox.js"></script>
        <script src="Assets/Library/Angular/angular-ui-bootstrap.js"></script>
        <script src="Assets/Scripts/loginRouteProvider.js"></script>
        <link rel="stylesheet" href="Assets/Library/Bootstrap/css/bootstrap.css">
        <link rel="stylesheet"
        	href="Assets/Library/Bootstrap/css/bootstrap-theme.css">
        <link rel="stylesheet" href="Assets/Styles/main.css">
        <link rel="stylesheet" href="Assets/Styles/bootstrap-checkbox-radio.css">
        <!-- Used for clear urls -->
        <!-- <base href="/smartiTest/index.php"> -->
    </head>
    <body ng-controller="loginController">
    	<div class="lang-bar-login">
    		<a href="#"><img class="lang" id="bg" src="Assets/Images/flag_bg.png"
    			alt="bg" title="{{strings.bg}}" /> </a> <a href="#"><img class="lang"
    			id="en" src="Assets/Images/flag_en.png" alt="en"
    			title="{{strings.en}}" /> </a> <a href="#"><img class="lang" id="de"
    			src="Assets/Images/flag_de.png" alt="de" title="{{strings.de}}" /> </a>
    	</div>
    	<div id="login-container">
    		<div class="col-md-12" id="header-login">
    			<h1 ng-bind-html="strings.system_heading" id="login-title"></h1>
    		</div>
    		<div class="col-md-12" id="loginContainer" ng-view></div>
    		<div class="col-md-12" id="footer-login">
    			<p id="login-footerText" ng-bind-html="strings.footer_text"></p>
    		</div>
    	</div>
    
    </body>
</html>


<?php
date_default_timezone_set('Europe/Sofia');
mb_internal_encoding('UTF-8');

define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'exam_system');

define('APPLICATION_SECRET_KEY', sha1('alexIsprEtTycoOolL'));

define('APPLICATION_ROOT_PATH', realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..') . DIRECTORY_SEPARATOR);
define('APPLICATION_ROOT_URL', str_replace($_SERVER['DOCUMENT_ROOT'], '', APPLICATION_ROOT_PATH));

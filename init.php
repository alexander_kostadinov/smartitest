<?php
session_start();
require_once('Core/Configuration.php');
require_once('Core/Database.php');
require_once('Core/Registry.php');
require_once('Core/State.php');
require_once('Core/ResourceHandler.php');

foreach (glob('Framework/Services/*.php') as $fileName) {
    require_once($fileName);
}

foreach (glob('Framework/Models/*.php') as $fileName) {
    require_once($fileName);
}

foreach (glob('Framework/Repositories/*.php') as $fileName) {
    require_once($fileName);
}

//create an instance of registry and add/set the resource and db objects
$registry = Registry::getInstance();

$registry->set('resource', new ResourceHandler());

$registry->set('database', new Database());

$registry->set('state', new State());

$_SESSION['reg'] = $registry;

//result, returned from the resourceHandler
$resource = $_SESSION['reg']->registry['resource'];

if (! empty($_POST['params'])) {
    $params = json_decode(stripslashes($_POST['params']));
    $serviceHandler = $resource->initResource($params->module, $params->unit);
    $resource->execute($serviceHandler, $params->method, $params->params);
    $result = $resource->getResponse();

    if (isset($result['username']) && isset($_SESSION['reg']->state['user']) && $result['username'] == $_SESSION['reg']->state['user']) {
        $_SESSION['username'] = $result['username'];
        $_SESSION['role'] = $result['role'];
        echo json_encode($result);
    } else {
        echo json_encode($result);
    }
}




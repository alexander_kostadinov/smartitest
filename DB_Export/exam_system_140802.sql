-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Време на генериране: 
-- Версия на сървъра: 5.5.27
-- Версия на PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- БД: `exam_system`
--

-- --------------------------------------------------------

--
-- Структура на таблица `test`
--

CREATE TABLE IF NOT EXISTS `test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `age` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=88 ;

--
-- Ссхема на данните от таблица `test`
--

INSERT INTO `test` (`id`, `name`, `age`) VALUES
(1, 'Sande', 23),
(2, 'Sande', 23),
(3, 'Sande', 23),
(4, 'Sande', 23),
(5, 'Sande', 23),
(6, 'Sande', 23),
(7, 'Sande', 23),
(8, 'Sande', 23),
(9, 'Petre', 22),
(10, 'Petre', 22),
(11, 'Petre', 22),
(12, 'Petre', 22),
(14, 'Petre', 22),
(16, 'Petre', 22),
(18, 'Petre', 22),
(19, 'Petre', 22),
(26, 'Petre', 22),
(27, 'Petre', 22),
(28, 'Petre', 23),
(29, 'Goshe', 25),
(30, 'Goshe', 25),
(31, 'Goshe', 25),
(32, 'Goshe', 25),
(33, 'Goshe', 25),
(34, 'Goshe', 25),
(35, 'Goshe', 25),
(36, 'Goshe', 25),
(37, 'Goshe', 25),
(38, 'Goshe', 25),
(39, 'Goshe', 25),
(40, 'Goshe', 25),
(41, 'Goshe', 25),
(42, 'Goshe', 25),
(43, 'Goshe', 25),
(44, 'Goshe', 25),
(45, 'Goshe', 25),
(46, 'Goshe', 25),
(47, 'Goshe', 25),
(48, 'Goshe', 25),
(49, 'Goshe', 25),
(50, 'Goshe', 25),
(51, 'Goshe', 25),
(52, 'Goshe', 25),
(53, 'Goshe', 25),
(54, 'Goshe', 25),
(55, 'Goshe', 25),
(56, 'Goshe', 25),
(57, 'Goshe', 25),
(58, 'Goshe', 25),
(59, 'Goshe', 25),
(60, 'Goshe', 25),
(61, 'Goshe', 25),
(62, 'Goshe', 25),
(63, 'Goshe', 25),
(64, 'Goshe', 25),
(65, 'Goshe', 25),
(66, 'Goshe', 25),
(67, 'Goshe', 25),
(68, 'Goshe', 25),
(69, 'Goshe', 25),
(70, 'Goshe', 25),
(71, 'Goshe', 25),
(72, 'Goshe', 25),
(73, 'Goshe', 25),
(74, 'Goshe', 25),
(75, 'Goshe', 25),
(76, 'Goshe', 25),
(77, 'Goshe', 25),
(78, 'Goshe', 25),
(79, 'Goshe', 25),
(80, 'Goshe', 25),
(81, 'Goshe', 25),
(82, 'Goshe', 25),
(83, 'Goshe', 25),
(84, 'Goshe', 25),
(85, 'Goshe', 25),
(86, 'Goshe', 25),
(87, 'Goshe', 25);

-- --------------------------------------------------------

--
-- Структура на таблица `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL COMMENT 'md5 encrypted ',
  `email` tinytext NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_locked` tinyint(1) NOT NULL COMMENT 'boolean - takes 0 or 1',
  `role` tinyint(1) unsigned NOT NULL COMMENT '0 - can create/do; 1 - can create; 2 - can do;',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Ссхема на данните от таблица `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`, `reg_date`, `is_locked`, `role`) VALUES
(1, 'sando', 'e5bd35a4b0ce75f40e6860357aa39875', 'sandokann@abv.bg', '2014-07-18 21:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Структура на таблица `user_login_attempt`
--

CREATE TABLE IF NOT EXISTS `user_login_attempt` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT 'User id from ''users'' table',
  `login_attempt_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Време на генериране: 
-- Версия на сървъра: 5.5.27
-- Версия на PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- БД: `exam_system`
--

-- --------------------------------------------------------

--
-- Структура на таблица `answer`
--

CREATE TABLE IF NOT EXISTS `answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(10) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `file` longblob,
  `is_correct` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Ссхема на данните от таблица `answer`
--

INSERT INTO `answer` (`id`, `question_id`, `title`, `file`, `is_correct`) VALUES
(1, 1, 'Einen Student', NULL, 0),
(2, 1, 'Einen Pianist', NULL, 1),
(3, 2, 'Klavierspielen', NULL, 1),
(4, 2, 'Photographie', NULL, 1),
(5, 2, 'Kuchen', NULL, 0),
(6, 3, 'Sofia', NULL, 1),
(7, 3, 'Polski Trambej', NULL, 0),
(8, 3, 'Trqvna', NULL, 0),
(9, 4, '1910', NULL, 1),
(10, 4, '1911', NULL, 0),
(11, 4, '1991', NULL, 0),
(12, 5, 'Nicola from Lombardy', NULL, 1),
(13, 5, 'Me', NULL, 0),
(14, 5, 'Chuck Norris', NULL, 0),
(15, 6, '156', NULL, 1),
(16, 6, '147', NULL, 1),
(17, 6, '146', NULL, 1),
(18, 6, 'Punto', NULL, 0);

-- --------------------------------------------------------

--
-- Структура на таблица `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `test_id` int(10) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `file` longblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Ссхема на данните от таблица `question`
--

INSERT INTO `question` (`id`, `test_id`, `title`, `file`) VALUES
(1, 1, 'Wer ist Alex?', NULL),
(2, 1, 'Meine Hobbys sind...?', NULL),
(3, 1, 'Ich bin aus...?', NULL),
(4, 2, 'When is Alfa Romeo founded?', NULL),
(5, 2, 'Who was Alfa''s founder?', NULL),
(6, 2, 'Which of the following are Alfa''s models?', NULL);

-- --------------------------------------------------------

--
-- Структура на таблица `test`
--

CREATE TABLE IF NOT EXISTS `test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `author_id` int(11) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Ссхема на данните от таблица `test`
--

INSERT INTO `test` (`id`, `title`, `author_id`, `date_created`, `date_modified`) VALUES
(1, 'Erste Pruefung von Alex', 1, '2014-09-23 16:33:42', NULL),
(2, 'Second test', 1, '2014-09-23 16:36:21', NULL);

-- --------------------------------------------------------

--
-- Структура на таблица `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL COMMENT 'md5 encrypted ',
  `email` tinytext NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_locked` tinyint(1) NOT NULL COMMENT 'boolean - takes 0 or 1',
  `role` tinyint(1) unsigned NOT NULL COMMENT '0 - can create/do; 1 - can create; 2 - can do;',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Ссхема на данните от таблица `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`, `reg_date`, `is_locked`, `role`) VALUES
(1, 'sando', 'e5bd35a4b0ce75f40e6860357aa39875', 'sandokann@abv.bg', '2014-07-18 21:00:00', 0, 0),
(15, 'Frank', '3ffcec792d5f611797674ffb44ff7870', 'sandokann@gmail.com', '2014-09-23 16:38:06', 0, 2);

-- --------------------------------------------------------

--
-- Структура на таблица `user_login_attempt`
--

CREATE TABLE IF NOT EXISTS `user_login_attempt` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT 'User id from ''users'' table',
  `login_attempt_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `user_test_result`
--

CREATE TABLE IF NOT EXISTS `user_test_result` (
  `user_id` int(10) unsigned NOT NULL,
  `test_id` int(10) unsigned NOT NULL,
  `last_score` int(10) unsigned NOT NULL,
  `best_score` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`test_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Ссхема на данните от таблица `user_test_result`
--

INSERT INTO `user_test_result` (`user_id`, `test_id`, `last_score`, `best_score`) VALUES
(1, 1, 25, 100),
(1, 2, 60, 100),
(15, 2, 40, 40);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

    var strings = {
        //language_bar
        bg: 'Bulgarisch',
        en: 'Englisch',
        de: 'Deutsch',
        //header & footer strings
        system_heading: 'Smarti Test System',
        welcome: 'Willkomen!',
        footer_text: 'Erstellt von Alexander Kostadinov, &copy; ' + new Date().getFullYear(),
        
        //error 404
        pageNotFound: 'Fehler 404 Page Not Found',
        pageNotFoundText: 'Die angeforderte Seite existiert nicht.',
        takeMeHome: 'Zurück zur Startseite',
       
        //main template menu strings and password modal
        home: '<i class="glyphicon glyphicon-home system-icon"></i>&nbsp;Home',
        homeBreadcrumb: 'Home',
        create_test: '<i class="glyphicon glyphicon-pencil system-icon"></i>&nbsp;Test erstellen',
        create_testBreadcrumb: 'Test erstellen',
        search_test: '<i class="glyphicon glyphicon-tags system-icon"></i>&nbsp;&nbsp;Test suchen',
        search_testBreadcrumb: 'Test suchen',
        logged_in: 'Hallo, ',
        logout: '<span class="glyphicon glyphicon-off"></span> Abmelden',
        exclamation_mark: '!',
        change_pass: '<span class="glyphicon glyphicon-lock"></span> Passwort Veränderung',
        change_pass_modal_heading: '<span class="glyphicon glyphicon-th-list"></span> Passwort Veränderung',
        close: '<span class="glyphicon glyphicon-ban-circle"></span>&nbsp;Abschließen',
        change: '<span class="glyphicon glyphicon-wrench"></span>&nbsp;Veränderung',
        old_pass: 'Altes Passwort...',
        new_pass: 'Neues Passwort...',
        new_pass_again: 'Neues Passwort Viederholung...',
        test_title: 'Test Titel...',
        del: '<span class="glyphicon glyphicon-trash"></span>&nbsp;Loeschen',
        //login menu strings
        login_button: '<span class="glyphicon glyphicon-log-in"></span>&nbsp;Anmelden',
        username_placeholder: 'Benutzername...',
        password_placeholder: 'Passwort...',
        forgotten_password: '<span class="glyphicon glyphicon-bell"></span>&nbsp;Sie haben das Passwort vergessen?',
        restore_pass_modal_heading: '<span class="glyphicon glyphicon-th-list"></span> Passwort Wiederherstellung',
        email_placeholder: 'Email...',
        register: '<span class="glyphicon glyphicon-flag"></span>&nbsp;Registrieren',
        reg_user_modal_heading: '<span class="glyphicon glyphicon-th-list"></span> Registrieren',
        role: 'Role: &nbsp;',
        roles: {
            1: '<span class="glyphicon glyphicon-cog"></span> Kann ein Test erstellen',
            2: '<span class="glyphicon glyphicon-cog"></span> Kann ein Test machen',
        },
       
        //login error strings -> mapped with the constants in backend
        login_errors: {
            1: 'Sie haben keine Benutzername gegeben.',
            2: 'Sie haben keinse Passwort gegeben.',
            3: 'Die gegebene Benutzername existiert nicht.',
            4: 'Falsches Passwort.',
            5: 'Ihre Kunde wird für 5 Minuten wegen mehrere erfolglose Anmeldung-Versuche gesperrt.',
            6: 'Ihre Kunde wird nun freigeschaltet, aber sie haben ein falsches Passwort eingegeben.'
        },
        
      //register_errors
        reg_errors: {
            1: 'Sie haben keine Benutzername gegeben.',
            2: 'Sie haben keinse Passwort gegeben.',
            3: 'Sie haben keines Email gegeben.',
            4: 'Das neues Passwort muss mindestens eine Großbuchstabe, eine Zahl und ein Sonderzeichen enthalten.',
            5: 'Das ist nicht ein Email.',
            6: 'Ein anderer Benutzer verwendet diese Benutzername.',
            7: 'Ein anderer Benutzer verwendet dieses Email.'
        },
        
        user_reg_success: 'Erfolgreiche Registrierung! Sie koenen in ihrer Kunde anmelden.',
       
        //create test strings
        create_test_button_label: '<span class="glyphicon glyphicon-plus"></span> Neues Test erstellen',
        number_of_questions_prompt: 'Bite geben Sie Anzahl von Fragen:',
        question_number_validation: 'Bitte den Anzahl von Fragen eintragen. Nur Ziffern möglich. ' + 
            'Maximaler Anzahl von Fragen: 60.',
        question_text: 'Text der Frage',
        add_question: '<span class="glyphicon glyphicon-plus-sign"></span> Neue Frage',
        finish_test: '<span class="glyphicon glyphicon-new-window"></span> Test schliessen',
        question: 'Frage',
        text: 'Text',
        file: 'File',
        add_answer: '<span class="glyphicon glyphicon-plus-sign"></span> Neue Antwort',
        answer: 'Antwort',
        right_answer: 'Richtige antwort?',
    
        //password change
        pass_change_errors: {
            1: 'Sie haben keines altes Passwort gegeben.',
            2: 'Sie haben keines neues Passwort gegeben.',
            3: 'Sie haben das neues Passwort nicht bestätigt.',
            4: 'Die neue Passworte übereinstimmen nicht.',
            5: 'Das neues Passwort muss mindestens eine Großbuchstabe, eine Zahl und ein Sonderzeichen enthalten.',
            6: 'Das altes Passwort ist falsch.',
        },
        pass_change_success: 'Sie haben das Passwort erflogreich verändert.',
        pass_restore_errors: {
            1: 'Sie haben keines Email gegeben.',
            2: 'Das ist nicht ein Email.',
            3: 'Sie haben keine Benutzername gegeben.',
            4: 'Falsche Kombination von Benutzername und Email.'
        },
        pass_restore_success: 'Wir schicken Ihnen ein Email mit der neuen Passwort.',
        browse: 'Waehlen...',
        delete_warning: '<span class="glyphicon glyphicon-exclamation-sign system-icon-big"></span>&nbsp;Sind Sie sicher, dass sie das Element loeschen wollen?',
        at_least_one_correct: '<span class="glyphicon glyphicon-exclamation-sign system-icon-big"></span>&nbsp;Mindestens eine richtige Antwort muss gewaehlt werden!',
        not_enough_questions: '<span class="glyphicon glyphicon-exclamation-sign system-icon-big"></span>&nbsp;Es gibt keine Antworten im Test.',
        not_enough_answers: '<span class="glyphicon glyphicon-exclamation-sign system-icon-big"></span>&nbsp;Im Test gibt es Fragen mit unzureichenden Anzahl von Antworten (mindestens 2).',
        no_test_title: '<span class="glyphicon glyphicon-exclamation-sign system-icon-big"></span>&nbsp;Test Titel nicht angegeben.',
        empty_test_fields: '<span class="glyphicon glyphicon-exclamation-sign system-icon-big"></span>&nbsp;Im Test gibt es Fragen oder Antworten mit leeren Text/Datei-Felder.',
        test_exists: '<span class="glyphicon glyphicon-exclamation-sign system-icon-big"></span>&nbsp;Test schon existiert!',
        test_success: 'Sie haben das Test erstellt.',
        your_tests: '<span class="glyphicon glyphicon-pushpin"></span> Ihre Tests',
        all_tests: '<span class="glyphicon glyphicon-file"></span> Alle Tests',
        search: 'Suchen...',
        show_more: 'Mehr Tests <span class="glyphicon glyphicon-arrow-down"></span>',
        title: '<span class=" glyphicon glyphicon-tasks"></span> Titel',
        author: '<span class=" glyphicon glyphicon-user"></span> Autor',
        number: '№',
        do_testBreadcrumb: 'Test machen',
        not_enough_answers_selected: '<span class="glyphicon glyphicon-exclamation-sign system-icon-big"></span>&nbsp;Im Test gibt es Fragen ohne richtige Antwort gegeben.',
        achieved: '<span class="glyphicon glyphicon-flag"></span> Sie haben ',
        points: ' Punkte bewirkt (%).',
        mystats_Breadcrumb: 'Mein Statistik',
        last_score: '<span class="glyphicon glyphicon-star"></span> Letzte Hinterlegung',
        best_score: '<span class="glyphicon glyphicon-star-empty"></span> Beste Hinterlegung',
        done_by_me: '<span class="glyphicon glyphicon-pushpin"></span> Ihre Hinterlegung',
        stats: '<span class="glyphicon glyphicon-th-list"></span> Mein Statistik',
    };
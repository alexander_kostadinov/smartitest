    var strings = {
        //language_bar
        bg: 'Български',
        en: 'Английски',
        de: 'Немски',
        //header & footer strings
        system_heading: 'Smarti Test System',
        welcome: 'Добре дошли!',
        footer_text: 'Created by Alexander Kostadinov, &copy; ' + new Date().getFullYear(),
        
        //error 404
        pageNotFound: 'Error 404 Page Not Fount',
        pageNotFoundText: 'Търсената страница не съществува.',
        takeMeHome: 'Обратно към начало',
       
        //main template menu strings and password modal
        home: '<i class="glyphicon glyphicon-home system-icon"></i>&nbsp;Начало',
        homeBreadcrumb: 'Начало',
        create_test: '<i class="glyphicon glyphicon-pencil system-icon"></i>&nbsp;Създаване на тест',
        create_testBreadcrumb: 'Създаване на тест',
        search_test: '<i class="glyphicon glyphicon-tags system-icon"></i>&nbsp;&nbsp;Търсене на тест',
        search_testBreadcrumb: 'Търсене на тест',
        logged_in: 'Здравейте, ',
        logout: '<span class="glyphicon glyphicon-off"></span> Изход',
        exclamation_mark: '!',
        change_pass: '<span class="glyphicon glyphicon-lock"></span> Смяна на парола',
        change_pass_modal_heading: '<span class="glyphicon glyphicon-th-list"></span> Смяна на парола',
        close: '<span class="glyphicon glyphicon-ban-circle"></span>&nbsp;Затвори',
        change: '<span class="glyphicon glyphicon-wrench"></span>&nbsp;Промяна',
        old_pass: 'Стара парола...',
        new_pass: 'Нова парола...',
        new_pass_again: 'Повторете новата парола...',
        test_title: 'Заглавие на теста...',
        del: '<span class="glyphicon glyphicon-trash"></span>&nbsp;Изтриване',
        //login menu strings
        login_button: '<span class="glyphicon glyphicon-log-in system"></span>&nbsp;Вход',
        username_placeholder: 'Потребителско име...',
        password_placeholder: 'Парола...',
        forgotten_password: '<span class="glyphicon glyphicon-bell"></span>&nbsp;Забравена парола?',
        restore_pass_modal_heading: '<span class="glyphicon glyphicon-th-list"></span> Възстановяване на парола',
        email_placeholder: 'Имейл...',
        register: '<span class="glyphicon glyphicon-flag"></span>&nbsp;Регистрация',
        reg_user_modal_heading: '<span class="glyphicon glyphicon-th-list"></span> Регистрация',
        role: 'Роля: &nbsp;',
        roles: {
            1: '<span class="glyphicon glyphicon-cog"></span> Може да създава тест',
            2: '<span class="glyphicon glyphicon-cog"></span> Може за прави тест',
        },
       
        //login error strings -> mapped with the constants in backend
        login_errors: {
            1: 'Не сте въвели потребителско име.',
            2: 'Не сте въвели парола.',
            3: 'Въведеното потребителско име не съществува.',
            4: 'Грешна парола.',
            5: 'Профилът ви е заключен за 5 минути поради няколко неуспешни опита за вход.',
            6: 'Профилът ви вече е отключен, но сте въвели грешна парола.'
        },
        
        //register_errors
        reg_errors: {
            1: 'Не сте въвели потребителско име.',
            2: 'Не сте въвели парола.',
            3: 'Не сте въвели имейл.',
            4: 'Новата парола трябва да има поне една главна буква, едно число и един специален символ.',
            5: 'Навалиден имейл.',
            6: 'Заето потребителско име.',
            7: 'Даденият имейл се използва от друг потребител.'
        },
        
        user_reg_success: 'Успешна регистрация! Можете да влезете в профила си.',
       
        //create test strings
        create_test_button_label: '<span class="glyphicon glyphicon-plus"></span> Създаване на нов тест',
        number_of_questions_prompt: 'Моля задайте брой въпроси:',
        question_number_validation: 'Моля въведете брой въпроси. Позволено е въвеждането единствено на цифри.' + 
            'Максимален брой въпроси: 60.',
        question_text: 'Текст на въпроса',
        add_question: '<span class="glyphicon glyphicon-plus-sign"></span> Добави въпрос',
        finish_test: '<span class="glyphicon glyphicon-new-window"></span> Завърши теста',
        question: 'Въпрос',
        text: 'Текст',
        file: 'Файл',
        add_answer: '<span class="glyphicon glyphicon-plus-sign"></span> Добави отговор',
        answer: 'Отговор',
        right_answer: 'Правилен отговор?',
        
        //password change/restore
        pass_change_errors: {
            1: 'Не сте въвели стара парола.',
            2: 'Не сте въвели нова парола.',
            3: 'Не сте потвърдили новата парола.',
            4: 'Потвърдената нова парола не съвпада.',
            5: 'Новата парола трябва да има поне една главна буква, едно число и един специален символ.',
            6: 'Грешна стара парола.',
        },
        pass_change_success: 'Успешно променихте паролата.',
        pass_restore_errors: {
            1: 'Не сте въвели имейл.',
            2: 'Навалиден имейл.',
            3: 'Не сте въвели потребителско име.',
            4: 'Грешна комбинация от потребителско име и имейл.'
        },
        pass_restore_success: 'Ще получите имейл с новата си парола.',
        browse: 'Избери...',
        delete_warning: '<span class="glyphicon glyphicon-exclamation-sign system-icon-big"></span>&nbsp;Сигурни ли сте, че искате за изтриете елемента?',
        at_least_one_correct: '<span class="glyphicon glyphicon-exclamation-sign system-icon-big"></span>&nbsp;За всеки въпрос следва да бъде избран поне един верен отговор!',
        not_enough_questions: '<span class="glyphicon glyphicon-exclamation-sign system-icon-big"></span>&nbsp;В теста няма добавени въпроси.',
        not_enough_answers: '<span class="glyphicon glyphicon-exclamation-sign system-icon-big"></span>&nbsp;В теста има въпроси с недостатъчен брой отговори (минимум 2).',
        no_test_title: '<span class="glyphicon glyphicon-exclamation-sign system-icon-big"></span>&nbsp;Не е посочено заглавие на теста.',
        empty_test_fields: '<span class="glyphicon glyphicon-exclamation-sign system-icon-big"></span>&nbsp;В теста има въпроси или отговори с непопълнени текстови/файлови полета.',
        test_exists: '<span class="glyphicon glyphicon-exclamation-sign system-icon-big"></span>&nbsp;Тест с такова заглавие вече съществува!',
        test_success: 'Тестът е въведен успешно.',
        your_tests: '<span class="glyphicon glyphicon-pushpin"></span> Ваши тестове',
        all_tests: '<span class="glyphicon glyphicon-file"></span> Всички тестове',
        search: 'Търсене...',
        show_more: 'Покажи още <span class="glyphicon glyphicon-arrow-down"></span>',
        title: '<span class=" glyphicon glyphicon-tasks"></span> Заглавие',
        author: '<span class=" glyphicon glyphicon-user"></span> Автор',
        number: '№',
        do_testBreadcrumb: 'Правене на тест',
        not_enough_answers_selected: '<span class="glyphicon glyphicon-exclamation-sign system-icon-big"></span>&nbsp;В теста има въпроси без нито един посочен верен отговор.',
        achieved: '<span class="glyphicon glyphicon-flag"></span> Постигнахте ',
        points: ' точки (%).',
        mystats_Breadcrumb: 'Лична статистика',
        last_score: '<span class="glyphicon glyphicon-star"></span> Последен резултат',
        best_score: '<span class="glyphicon glyphicon-star-empty"></span> Най-добър резултат',
        done_by_me: '<span class="glyphicon glyphicon-pushpin"></span> Ваши резултати',
        stats: '<span class="glyphicon glyphicon-th-list"></span> Лична статистика',
    };
'use strict';
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = 'expires=' + d.toGMTString();
    document.cookie = cname + '=' + cvalue + '; ' + expires;
}

function getCookie(cname) {
    var name = cname + '=';
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
    }
    return undefined;
}
if (getCookie('lang') == undefined) {
    setCookie('lang', 'bg', 1);
}

document.writeln('<script charset="utf-8" src="Assets/Scripts/stringProvider_'+ getCookie('lang') +'.js"></script>');
document.writeln('<script src="Assets/Scripts/Controllers/loginController.js"></script>');

var myApp = angular.module('smarti', ['ngRoute','ngSanitize','ui.bootstrap']);
var ajaxUrl = '/smartiTest/init.php';

myApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider.when('/login', {
        templateUrl: 'Assets/Templates/login.html',
        controller: 'loginController'
      }).otherwise({
        redirectTo: '/login'
      });
    
    // use the HTML5 History API: added base url in index.php - no back functionality
    //$locationProvider.html5Mode(true);
}]);

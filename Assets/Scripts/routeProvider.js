'use strict';
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = 'expires=' + d.toGMTString();
    document.cookie = cname + '=' + cvalue + '; ' + expires;
}

function getCookie(cname) {
    var name = cname + '=';
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
    }
    return '';
}

document.writeln('<script charset="utf-8" src="Assets/Scripts/stringProvider_'+ getCookie('lang') +'.js"></script>');
document.writeln('<script src="Assets/Scripts/Controllers/mainController.js"></script>');
document.writeln('<script src="Assets/Scripts/Controllers/createTestController.js"></script>');
document.writeln('<script src="Assets/Scripts/Controllers/doTestController.js"></script>');

var myApp = angular.module('smarti', ['ngRoute','ngSanitize','ui.bootstrap','angularFileUpload', 'ng-breadcrumbs']);
var ajaxUrl = '/smartiTest/init.php';

myApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider.when('/home', {
        templateUrl: 'Assets/Templates/home.html',
        controller: 'mainController',
        label: strings.homeBreadcrumb
      }).when('/createTest', {
        templateUrl: 'Assets/Templates/CreateTest.html',
        controller: 'createTestController',
        label: strings.create_testBreadcrumb
      }).when('/doTest', {
          templateUrl: 'Assets/Templates/DoTest.html',
          controller: 'doTestController',
          label: strings.search_testBreadcrumb
      }).when('/doTestBody/:testId', {
          templateUrl: 'Assets/Templates/DoTestBody.html',
          controller: 'doTestController',
          label: strings.do_testBreadcrumb
      }).when('/myStats', {
          templateUrl: 'Assets/Templates/MyStats.html',
          controller: 'doTestController',
          label: strings.mystats_Breadcrumb
      }).otherwise({
        redirectTo: '/home'
      });
    
 // use the HTML5 History API: added base url in main.php - no back functionality
    //$locationProvider.html5Mode(true);
}]);

    var strings = {
        //language_bar
        bg: 'Bulgarian',
        en: 'English',
        de: 'German',
        //header & footer strings
        system_heading: 'Smarti Test System',
        welcome: 'Welcome!',
        footer_text: 'Created by Alexander Kostadinov, &copy; ' + new Date().getFullYear(),
        
        //error 404
        pageNotFound: 'Error 404 Page Not Found',
        pageNotFoundText: 'The requested page does not exist.',
        takeMeHome: 'Back to home',
       
        //main template menu strings and password modal
        home: '<i class="glyphicon glyphicon-home system-icon"></i>&nbsp;Home',
        homeBreadcrumb: 'Home',
        create_test: '<i class="glyphicon glyphicon-pencil system-icon"></i>&nbsp;Create Test',
        create_testBreadcrumb: 'Search Test',
        search_test: '<i class="glyphicon glyphicon-tags system-icon"></i>&nbsp;&nbsp;Search Test',
        search_testBreadcrumb: 'Test suchen',
        logged_in: 'Hello, ',
        logout: '<span class="glyphicon glyphicon-off"></span> Logout',
        exclamation_mark: '!',
        change_pass: '<span class="glyphicon glyphicon-lock"></span> Change password',
        change_pass_modal_heading: '<span class="glyphicon glyphicon-th-list"></span> Change password',
        close: '<span class="glyphicon glyphicon-ban-circle"></span>&nbsp;Close',
        change: '<span class="glyphicon glyphicon-wrench"></span>&nbsp;Change',
        old_pass: 'Old password...',
        new_pass: 'New password...',
        new_pass_again: 'Confirm new password...',
        test_title: 'Test Title...',
        del: '<span class="glyphicon glyphicon-trash"></span>&nbsp;Remove',
        //login menu strings
        login_button: '<span class="glyphicon glyphicon-log-in"></span>&nbsp;Login',
        username_placeholder: 'Username...',
        password_placeholder: 'Password...',
        forgotten_password: '<span class="glyphicon glyphicon-bell"></span>&nbsp;Forgotten password?',
        restore_pass_modal_heading: '<span class="glyphicon glyphicon-th-list"></span> Restore password',
        email_placeholder: 'Email...',
        register: '<span class="glyphicon glyphicon-flag"></span>&nbsp;Registration',
        reg_user_modal_heading: '<span class="glyphicon glyphicon-th-list"></span> Registration',
        role: 'Role: &nbsp;',
        roles: {
            1: '<span class="glyphicon glyphicon-cog"></span> Can create test',
            2: '<span class="glyphicon glyphicon-cog"></span> Can do test',
        },
        
        //login error strings -> mapped with the constants in backend
        login_errors: {
            1: 'You have not entered a username.',
            2: 'You have not entered a password.',
            3: 'The given username does not exist.',
            4: 'Wrong password.',
            5: 'Your account is locked for 5 minutes due to several unsuccessful login attempts.',
            6: 'Your account is already unlocked, but you have entered a wrong password.'
        },
        
      //register_errors
        reg_errors: {
            1: 'You have not entered a username.',
            2: 'You have not entered a password.',
            3: 'You have not entered an email.',
            4: 'The new password must contain at least one capital letter, one number and a special symbol.',
            5: 'Invalid email.',
            6: 'Username already taken.',
            7: 'The given email is used by another user.'
        },
        
        user_reg_success: 'Registration successful! You can now log in your account.',
       
        //create test strings
        create_test_button_label: '<span class="glyphicon glyphicon-plus"></span> Create new test',
        number_of_questions_prompt: 'Please enter number of questions:',
        question_number_validation: 'Please enter number of questions. Only integer input allowed. Maximal number of questions: 60',
        question_text: 'Question text',
        add_question: '<span class="glyphicon glyphicon-plus-sign"></span> Add question',
        finish_test: '<span class="glyphicon glyphicon-new-window"></span> Finalize test',
        question: 'Question',
        text: 'Text',
        file: 'File',
        add_answer: '<span class="glyphicon glyphicon-plus-sign"></span> Add an answer',
        answer: 'Answer',
        right_answer: 'Correct answer?',
    
        //password change
        pass_change_errors: {
            1: 'You have not entered the old password.',
            2: 'You have not entered a new password.',
            3: 'You have not confirmed the new password.',
            4: 'The confirmed password does not match.',
            5: 'The new password must contain at least one capital letter, one number and a special symbol.',
            6: 'Wrong old password.',
        },
        pass_change_success: 'Password changed successfully.',
        pass_restore_errors: {
            1: 'You have not entered an email.',
            2: 'Invalid email.',
            3: 'You have not entered a username.',
            4: 'Wrong combination of username and password.'
        },
        pass_restore_success: 'You will recieve an email with your new password.',
        browse: 'Browse...',
        delete_warning: '<span class="glyphicon glyphicon-exclamation-sign system-icon-big"></span>&nbsp;Are you sure you want to delete this element?',
        at_least_one_correct: '<span class="glyphicon glyphicon-exclamation-sign system-icon-big"></span>&nbsp;At least one correct answer must be checked for each question!',
        not_enough_questions: '<span class="glyphicon glyphicon-exclamation-sign system-icon-big"></span>&nbsp;There are no questions in the test.',
        not_enough_answers: '<span class="glyphicon glyphicon-exclamation-sign system-icon-big"></span>&nbsp;There are test questions with not enough answers (min. 2).',
        no_test_title: '<span class="glyphicon glyphicon-exclamation-sign system-icon-big"></span>&nbsp;No test title given.',
        empty_test_fields: '<span class="glyphicon glyphicon-exclamation-sign system-icon-big"></span>&nbsp;The test contains empty text/file input fields.',
        test_exists: '<span class="glyphicon glyphicon-exclamation-sign system-icon-big"></span>&nbsp;Test with the given title already exists!',
        test_success: 'Test created successfully.',
        your_tests: '<span class="glyphicon glyphicon-pushpin"></span> Your tests',
        all_tests: '<span class="glyphicon glyphicon-file"></span> All tests',
        search: 'Search...',
        show_more: 'Show more <span class="glyphicon glyphicon-arrow-down"></span>',
        title: '<span class=" glyphicon glyphicon-tasks"></span> Title',
        author: '<span class=" glyphicon glyphicon-user"></span> Author',
        number: '№',
        do_testBreadcrumb: 'Do test',
        not_enough_answers_selected: '<span class="glyphicon glyphicon-exclamation-sign system-icon-big"></span>&nbsp;The test contains questions without a given right answer.',
        achieved: '<span class="glyphicon glyphicon-flag"></span> You scored ',
        points: ' points (%).',
        mystats_Breadcrumb: 'Personal statistics',
        last_score: '<span class="glyphicon glyphicon-star"></span> Last score',
        best_score: '<span class="glyphicon glyphicon-star-empty"></span> Best score',
        done_by_me: '<span class="glyphicon glyphicon-pushpin"></span> Your results',
        stats: '<span class="glyphicon glyphicon-th-list"></span> Personal statistics',
    };
'use strict';

var myApp = angular.module('smarti');

myApp.controller('mainController', ['$scope', 'breadcrumbs','$route', '$routeParams', '$location', '$rootScope', '$sce', '$compile', '$http', '$modal', function($scope, breadcrumbs, $route, $routeParams, $location, $rootScope, $sce, $compile, $http, $modal) {
    //string initializer
    $scope.strings = strings;
    $scope.breadcrumbs = breadcrumbs;
    
	/**
     * Initializes the main page after login - buttons, menus, user info - what's hidden and not
     * @author alexander.kostadinov
     */
	$scope.initHome = function() {  
	    $('.lang').click(function (event) {  
	        setCookie('lang', event.target.id, 1);
	        window.location.replace('/smartiTest/main.php');
	    });
	    
	    $('li[name="create-menu-item"]').removeClass('active');
	    $('li[name="do-menu-item"]').removeClass('active');
	    $('li[name="home-menu-item"]').addClass('active');
	    
	    $scope.showLogout = false;
	    $scope.showPassChange = false;
	    $scope.showMyStats = false;
	    $scope.showCreateTest = false;
        $scope.showDoTest = false;
	    var params = {
                module: 'login',
                unit: 'service',
                method: 'readUserInfo',
                params: {}
            };
        $http({
            method: 'POST',
            url: ajaxUrl,
            data: 'params=' + JSON.stringify(params),
            headers : {'Content-Type': 'application/x-www-form-urlencoded'},
            cache: false
        }).success(function(data) { 
            $scope.displayUserName = '<span class="glyphicon glyphicon-user system-icon"></span>&nbsp;&nbsp;' + data.username;
            switch (data.role) {
                case 0:
                    $scope.showCreateTest = true;
                    $scope.showDoTest = true;
                    break;
                case 1:
                    $scope.showCreateTest = true;
                    $scope.showDoTest = false;
                    break;
                case 2:
                    $scope.showCreateTest = false;
                    $scope.showDoTest = true;
                    break;
            }
            $scope.showLogout = true;
            $scope.showPassChange = true;
            $scope.showMyStats = true;
        });
	};
	/**
     * Logs the user out
     * @author alexander.kostadinov
     */
	
	$scope.doLogOut = function() {
	    var params = {
                module: 'login',
                unit: 'service',
                method: 'doLogout',
                params: {}
            };
        $http({
            method: 'POST',
            url: ajaxUrl,
            data: 'params=' + JSON.stringify(params),
            headers : {'Content-Type': 'application/x-www-form-urlencoded'},
            cache: false
        }).success(function(data) { 
            if (data.hasLoggedOut !== undefined && data.hasLoggedOut == true) {
                window.location.href = 'index.php';
            }
        });
	};
	/**
     * Opens a change pass modal and does password change
     * @author alexander.kostadinov
     */
    
    $scope.doChangePass = function(action) {
        $scope.openChangePassModal('lg', action);
    };
    
    $scope.openChangePassModal = function (size, action) {
        
        switch (action) {
            case 'open':  
                var modalInstance = $modal.open({
                    templateUrl: 'Assets/Templates/ChangePassModal.html',
                    controller: 'mainController',
                    size: size
                  });
                $scope.$parent.modalInstance = modalInstance;
                break;
            case 'confirm':  
                    var params = {
                            module: 'user',
                            unit: 'service',
                            method: 'doChangePass',
                            params: {
                                'oldPass': $scope.oldPass,
                                'newPass' : $scope.newPass,
                                'newPassAgain' : $scope.newPassAgain
                            }
                        };
                    $http({
                        method: 'POST',
                        url: ajaxUrl,
                        data: 'params=' + JSON.stringify(params),
                        headers : {'Content-Type': 'application/x-www-form-urlencoded'},
                        cache: false
                    }).success(function(data) { 
                        $('#old-pass-group').removeClass('has-error');
                        $('#new-pass-group').removeClass('has-error');
                        $('#new-pass-again-group').removeClass('has-error');
                        //empties all ids containing error-
                        $("[id*='pass-error-']").empty();
                        
                        if (data !== 'true') {
                            for (var err in data) {
                                
                                switch (data[err]) {
                                    case 1:
                                        $('#old-pass-group').addClass('has-error');
                                        $('#pass-error-' + err).fadeIn(300).append('<span class="help-block "></span>'
                                                + $scope.strings.pass_change_errors[err]);
                                        if ($('#new-pass-group').not('.has-error')) {
                                            $('#new-pass-group').addClass('has-success');
                                        }
                                        if ($('#new-pass-again-group').not('.has-error')) {
                                            $('#new-pass-again-group').addClass('has-success');
                                        }
                                        break;
                                    case 2:
                                        $('#new-pass-group').addClass('has-error');
                                        $('#pass-error-' + err).fadeIn(300).append('<span class="help-block "></span>'
                                                + $scope.strings.pass_change_errors[err]);
                                        if ($('#old-pass-group').not('.has-error')) {
                                            $('#old-pass-group').addClass('has-success');
                                        }
                                        if ($('#new-pass-again-group').not('.has-error')) {
                                            $('#new-pass-again-group').addClass('has-success');
                                        }
                                        break;
                                    case 3:
                                        $('#new-pass-again-group').addClass('has-error');
                                        $('#pass-error-' + err).fadeIn(300).append('<span class="help-block "></span>'
                                                + $scope.strings.pass_change_errors[err]);
                                        if ($('#old-pass-group').not('.has-error')) {
                                            $('#old-pass-group').addClass('has-success');
                                        }
                                        if ($('#new-pass-group').not('.has-error')) {
                                            $('#new-pass-group').addClass('has-success');
                                        }
                                        break;
                                    case 4:
                                        $('#new-pass-again-error').empty();
                                        $('#new-pass-group').addClass('has-error');
                                        $('#new-pass-again-group').addClass('has-error');
                                        $('#pass-error-' + err).fadeIn(300).append('<span class="help-block"></span>'
                                                + $scope.strings.pass_change_errors[err]);
                                        $('#new-pass-again-group').addClass('has-error');
                                        if ($('#old-pass-group').not('.has-error')) {
                                            $('#old-pass-group').addClass('has-success');
                                        }
                                        break;
                                    case 5:
                                        $('#new-pass-again-error').empty();
                                        $('#new-pass-group').addClass('has-error');
                                        $('#new-pass-again-group').addClass('has-error');
                                        $('#pass-error-' + err).fadeIn(300).append('<span class="help-block"></span>'
                                                + $scope.strings.pass_change_errors[err]);
                                        if ($('#old-pass-group').not('.has-error')) {
                                            $('#old-pass-group').addClass('has-success');
                                        }
                                        break;
                                    case 6:
                                        $('#old-pass-group').addClass('has-error');
                                        $('#pass-error-' + err).fadeIn(300).append('<span class="help-block"></span>'
                                                + $scope.strings.pass_change_errors[err]);
                                        if ($('#new-pass-group').not('.has-error')) {
                                            $('#new-pass-group').addClass('has-success');
                                        }
                                        if ($('#new-pass-again-group').not('.has-error')) {
                                            $('#new-pass-again-group').addClass('has-success');
                                        }
                                        break;
                                }
                            }
                        } else {
                            $('#change-pass-modal-body').empty();
                            $('#change-pass-modal-body').append('<div class="alert alert-success" role="alert"><p><span class="glyphicon glyphicon-ok"></span>&nbsp;'
                                    + $scope.strings.pass_change_success + '</p></div>');
                            $('#change-pass-button').remove();
                        }
                    });
                break;
            case 'cancel':
                $scope.modalInstance.close();
                break;
        }
    };
}]);

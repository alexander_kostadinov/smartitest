'use strict';

var myApp = angular.module('smarti');

myApp.controller('loginController', ['$scope', '$route', '$routeParams', '$location', '$rootScope', '$sce', '$compile', '$http', '$modal', function($scope, $route, $routeParams, $location, $rootScope, $sce, $compile, $http, $modal) {
    //string initializer
    $scope.strings = strings;
    
    /**
     * Initializes the login page - hides elements, etc.
     * @author alexander.kostadinov
     */
    $scope.initLogin = function() {
        $('.lang').click(function (event) {  
        setCookie('lang', event.target.id, 1);
        window.location.replace('/smartiTest/index.php');
    });
        $('#login-error-messages').hide();
        $('#username-group').removeClass('has-error');
        $('#password-group').removeClass('has-error');
        $('form:first *:input[type!=hidden]:first').focus();
        $('#loader-div').hide();
    };
    
    /**
     * Does the login (backend validation included)
     * @author alexander.kostadinov
     */
    $scope.doLogin = function() {
        $('#login-error-messages').hide();
        $('#username-group').removeClass('has-error');
        $('#password-group').removeClass('has-error');
        $('#loader-div').fadeIn(150);
        var params = {
                module: 'login',
                unit: 'service',
                method: 'doLogin',
                params: {
                    username: $scope.username,
                    password: $scope.password
                }
            };
        
        $http({
            method: 'POST',
            url: ajaxUrl,
            data: 'params=' + JSON.stringify(params),
            headers : {'Content-Type': 'application/x-www-form-urlencoded'},
            cache: false
        }).success(function(data) {  
            $('#loader-div').fadeOut(600);
            if (data.username !== undefined) { console.log(data);
                $('#login-error-messages').hide();
                window.location.href = 'main.php';
                return;
            }
            
            $('#login-error-messages').empty();
            
            if (data !== 'false') {
                for (var err in data) {
                    $('#login-error-messages').fadeIn(300).append('<p><span class="glyphicon glyphicon-remove"></span>&nbsp;'
                                                + $scope.strings.login_errors[err]+ '</p>');
                    switch (data[err]) {
                        case 1:
                            $('#username-group').addClass('has-error');
                            if ($('#password-group').not('.has-error')) {
                                $('#password-group').addClass('has-success');
                            }
                            break;
                        case 2:
                            $('#password-group').addClass('has-error');
                            if ($('#username-group').not('.has-error')) {
                                $('#username-group').addClass('has-success');
                            }
                            break;
                        case 3:
                            $('#username-group').addClass('has-error');
                            if ($('#password-group').not('.has-error')) {
                                $('#password-group').addClass('has-success');
                            }
                            break;
                        case 4:
                            $('#password-group').addClass('has-error');
                            if ($('#username-group').not('.has-error')) {
                                $('#username-group').addClass('has-success');
                            }
                            break;
                        case 5:
                            $('#username-group').removeClass('has-error');
                            $('#password-group').removeClass('has-error');
                            $('#username-group').removeClass('has-success');
                            $('#password-group').removeClass('has-success');
                            break;
                        case 6:
                            $('#username-group').removeClass('has-error');
                            $('#password-group').removeClass('has-error');
                            $('#username-group').removeClass('has-success');
                            $('#password-group').removeClass('has-success');
                            break;
                    }
                    
                    
                }
            } else {
                $('#login-error-messages').fadeIn(300).append('<p><span class="glyphicon glyphicon-remove"></span>&nbsp;'
                                            + $scope.strings.login_errors[5]+ '</p>');
                $('#username-group').removeClass('has-error');
                $('#password-group').removeClass('has-error');
                $('#username-group').removeClass('has-success');
                $('#password-group').removeClass('has-success');
            }
        });
    };
    
    /**
     * Restores password (backend validation included)
     * @author alexander.kostadinov
     */
    $scope.doRestorePassword = function(action) {
        $scope.openRestorePassModal('lg', action);
    };
    
    /**
     * Opens modal window for password restore (backend validation included)
     * @author alexander.kostadinov
     */
    $scope.openRestorePassModal = function (size, action) {
        switch (action) {
            case 'open':  
                var modalInstance = $modal.open({
                    templateUrl: 'Assets/Templates/RestorePassModal.html',
                    controller: 'loginController',
                    size: size
                  });
                //XXX Cannot be done with $scope.$parent - unknown reason
                $rootScope.modalInstance = modalInstance;
                break;
            case 'confirm':  
                    $('#restore-pass-email-group').removeClass('has-error');
                    $('#restore-pass-username-group').removeClass('has-error');
                    $('#restore-pass-email-error').empty();
                    $('#restore-pass-username-error').empty();
                    var params = {
                            module: 'user',
                            unit: 'service',
                            method: 'doRestorePass',
                            params: {
                                'email': $scope.restorePassEmail,
                                'username': $scope.restorePassUsername
                            }
                        };
                    $http({
                        method: 'POST',
                        url: ajaxUrl,
                        data: 'params=' + JSON.stringify(params),
                        headers : {'Content-Type': 'application/x-www-form-urlencoded'},
                        cache: false
                    }).success(function(data) { 
                        
                        if (data !== 'true') {
                            for (var err in data) {
                                switch (data[err]) {
                                    case 1:
                                        $('#restore-pass-email-group').addClass('has-error');
                                        $('#restore-pass-email-error').fadeIn(300).append('<span class="help-block "></span>'
                                                + $scope.strings.pass_restore_errors[err]);
                                        if ($('#restore-pass-username-group').not('.has-error')) {
                                            $('#restore-pass-username-group').addClass('has-success');
                                        }
                                        break;
                                    case 2:
                                        $('#restore-pass-email-group').addClass('has-error');
                                        $('#restore-pass-email-error').fadeIn(300).append('<span class="help-block "></span>'
                                                + $scope.strings.pass_restore_errors[err]);
                                        if ($('#restore-pass-username-group').not('.has-error')) {
                                            $('#restore-pass-username-group').addClass('has-success');
                                        }
                                        break;
                                    case 3:
                                        $('#restore-pass-username-group').addClass('has-error');
                                        $('#restore-pass-username-error').fadeIn(300).append('<span class="help-block "></span>'
                                                + $scope.strings.pass_restore_errors[err]);
                                        if ($('#restore-pass-email-group').not('.has-error')) {
                                            $('#restore-pass-email-group').addClass('has-success');
                                        }
                                        break;
                                    case 4:
                                        $('#restore-pass-username-group').addClass('has-error');
                                        $('#restore-pass-email-group').addClass('has-error');
                                        $('#restore-pass-username-error').fadeIn(300).append('<span class="help-block "></span>'
                                                + $scope.strings.pass_restore_errors[err]);
                                        break;
                                }
                            }
                        } else {
                            $('#restore-pass-modal-body').empty();
                            $('#restore-pass-modal-body').append('<div class="alert alert-success" role="alert"><p><span class="glyphicon glyphicon-ok"></span>&nbsp;'
                                    + $scope.strings.pass_restore_success + '</p></div>');
                            $('#restore-pass-button').remove();
                        }
                    });
                break;
            case 'cancel':
                $rootScope.modalInstance.close();
                break;
        }
    }
    
    /**
     * Registers a user
     * @author alexander.kostadinov
     */
    $scope.doRegister = function(action) {
        $scope.openRegisterModal('lg', action);
    };
    
    /**
     * Gets the role field value or sets it if necessary
     * @author alexander.kostadinov
     */
    $scope.getRole = function(role) {
        
        if (role == 1) {
            $('#role1').addClass('active');
            $('#role2').removeClass('active');
        }
        
        if (role == 2) {
            $('#role2').addClass('active');
            $('#role1').removeClass('active');
        }
        
        $scope.selectedRole = $scope.strings.roles[role];
        $scope.roleId = role;
    }
    
    /**
     * Opens the register menu modal
     * @author alexander.kostadinov
     */
    $scope.openRegisterModal = function (size, action) {
        switch (action) {
            case 'open':  
                var modalInstance = $modal.open({
                    templateUrl: 'Assets/Templates/RegisterUserModal.html',
                    controller: 'loginController',
                    size: size
                  });
                //XXX Cannot be done with $scope.$parent - unknown reason
                $rootScope.modalInstance = modalInstance;
                $rootScope.selectedRole = $scope.strings.roles[1];
                break;
            case 'confirm':  
                if ($scope.roleId == undefined) {
                    $scope.getRole(1);
                }
                $('#reg-user-username-group').removeClass('has-error');
                $('#reg-user-pass-group').removeClass('has-error');
                $('#reg-user-email-group').removeClass('has-error');
                $('#reg-user-username-error').empty();
                $('#reg-user-pass-error').empty();
                $('#reg-user-email-error').empty();
                $('#reg-user-general-error').empty();
                var params = {
                        module: 'user',
                        unit: 'service',
                        method: 'doRegisterUser',
                        params: {
                            'email': $scope.regUserEmail,
                            'username': $scope.regUserUsername,
                            'password': $scope.regUserPass,
                            'role': $scope.roleId
                        }
                    };
                $http({
                    method: 'POST',
                    url: ajaxUrl,
                    data: 'params=' + JSON.stringify(params),
                    headers : {'Content-Type': 'application/x-www-form-urlencoded'},
                    cache: false
                }).success(function(data) { 
                    if (data !== 'true') {
                        for (var err in data) {
                            switch (data[err]) {
                                case 1:
                                    $('#reg-user-username-group').addClass('has-error');
                                    $('#reg-user-username-error').fadeIn(300).append('<span class="help-block "></span>'
                                            + $scope.strings.reg_errors[err]);
                                    if ($('#reg-user-pass-group').not('.has-error')) {
                                        $('#reg-user-pass-group').addClass('has-success');
                                    }
                                    if ($('#reg-user-email-group').not('.has-error')) {
                                        $('#reg-user-email-group').addClass('has-success');
                                    }
                                    break;
                                case 2:
                                    $('#reg-user-pass-group').addClass('has-error');
                                    $('#reg-user-pass-error').fadeIn(300).append('<span class="help-block "></span>'
                                            + $scope.strings.reg_errors[err]);
                                    if ($('#reg-user-username-group').not('.has-error')) {
                                        $('#reg-user-username-group').addClass('has-success');
                                    }
                                    if ($('#reg-user-email-group').not('.has-error')) {
                                        $('#reg-user-email-group').addClass('has-success');
                                    }
                                    break;
                                case 3:
                                    $('#reg-user-email-group').addClass('has-error');
                                    $('#reg-user-email-error').fadeIn(300).append('<span class="help-block "></span>'
                                            + $scope.strings.reg_errors[err]);
                                    if ($('#reg-user-username-group').not('.has-error')) {
                                        $('#reg-user-username-group').addClass('has-success');
                                    }
                                    if ($('#reg-user-pass-group').not('.has-error')) {
                                        $('#reg-user-pass-group').addClass('has-success');
                                    }
                                    break;
                                case 4:
                                    $('#reg-user-pass-group').addClass('has-error');
                                    $('#reg-user-pass-error').fadeIn(300).append('<span class="help-block "></span>'
                                            + $scope.strings.reg_errors[err]);
                                    if ($('#reg-user-username-group').not('.has-error')) {
                                        $('#reg-user-username-group').addClass('has-success');
                                    }
                                    if ($('#reg-user-email-group').not('.has-error')) {
                                        $('#reg-user-email-group').addClass('has-success');
                                    }
                                    break;
                                case 5:
                                    $('#reg-user-email-group').addClass('has-error');
                                    $('#reg-user-email-error').fadeIn(300).append('<span class="help-block "></span>'
                                            + $scope.strings.reg_errors[err]);
                                    if ($('#reg-user-username-group').not('.has-error')) {
                                        $('#reg-user-username-group').addClass('has-success');
                                    }
                                    if ($('#reg-user-pass-group').not('.has-error')) {
                                        $('#reg-user-pass-group').addClass('has-success');
                                    }
                                    break;
                                case 6:
                                    $('#reg-user-username-group').addClass('has-error');
                                    $('#reg-user-pass-group').addClass('has-error');
                                    $('#reg-user-email-group').addClass('has-error');
                                    $('#reg-user-general-error').fadeIn(300).append('<span class="help-block "></span>'
                                            + $scope.strings.reg_errors[err]);
                                    break;
                                case 7:
                                    $('#reg-user-username-group').addClass('has-error');
                                    $('#reg-user-pass-group').addClass('has-error');
                                    $('#reg-user-email-group').addClass('has-error');
                                    $('#reg-user-general-error').fadeIn(300).append('<span class="help-block "></span>'
                                            + $scope.strings.reg_errors[err]);
                                    break;
                            }
                        }
                    } else {
                        $('#reg-user-modal-body').empty();
                        $('#reg-user-modal-body').append('<div class="alert alert-success" role="alert"><p><span class="glyphicon glyphicon-ok"></span>&nbsp;'
                                + $scope.strings.user_reg_success + '</p></div>');
                        $('#reg-user-button').remove();
                    }
                });
                break;
            case 'cancel':
                $rootScope.modalInstance.close();
                break;
        }
    }
}]);
'use strict';

var myApp = angular.module('smarti');

myApp.factory('itemService', function() {
    return {
        getAll : function(data) {
            var items = [];
            for (var item in data) {
                items.push(data[item]);
            }
            return items;
        }
    };
});

myApp.controller('doTestController', ['$scope', '$route', '$routeParams', '$location', '$rootScope', '$sce', '$compile', '$http', '$upload', 'itemService', function($scope, $route, $routeParams, $location, $rootScope, $sce, $compile, $http, $upload, itemService) {
    //string initializer
    $scope.strings = strings;
    
    $scope.init = function() {
        $('li[name="home-menu-item"]').removeClass('active');
        $('li[name="create-menu-item"]').removeClass('active');
        $('li[name="do-menu-item"]').addClass('active');
        $scope.items = [];
        var params = {
            module: 'create',
            unit: 'service',
            method: 'getAllTestsByUserId',
            params: {}
        };
        
        $http({
            method: 'POST',
            url: ajaxUrl,
            data: 'params=' + JSON.stringify(params),
            headers : {'Content-Type': 'application/x-www-form-urlencoded'},
            cache: false
        }).success(function(data) { 
            if (data.length !== 0 && data !== '3') {
                var container = $('<div id="own-tests" class="panel panel-default"></div><br />');
                var title = $('<div class="panel-heading"><h4>' + $scope.strings.your_tests + '</h4></div><br />');
                $(container).append(title);
                
                for (var el in data) {
                    var test = data[el];
                    var testNumber = el;
                    testNumber++;
                    var testElement = $('<p class="test-link">' + testNumber + '. <a href=#/editTest/' + test.id + '>' + test.title + '</a>&nbsp;[&nbsp;' + test.date_created + '&nbsp;]</p>');
                    $(container).append(testElement);
                }
                $(container).insertBefore('#testTitleContainer');
            }
        });
        
        var paramsAll = {
                module: 'create',
                unit: 'service',
                method: 'getAllTests',
                params: {}
        };
        
        $http({
            method: 'POST',
            url: ajaxUrl,
            data: 'params=' + JSON.stringify(paramsAll),
            headers : {'Content-Type': 'application/x-www-form-urlencoded'},
            cache: false
        }).success(function(data) { 
            
            if (data.length !== 0) {
                $scope.items = itemService.getAll(data);
            }
        });
    };
    
    var pagesShown = 1;
    var pageSize = 5;
    
    $scope.itemsLimitStats = function() {
        return pageSize * pagesShown;
    };
    $scope.hasMoreItemsToShowStats = function() {
        return pagesShown < ($scope.itemsStats.length / pageSize);
    };
    $scope.showMoreItemsStats = function() {
        pagesShown = pagesShown + 1;
    };
    
    $scope.initMyStats = function() {

        $scope.itemsStats = [];
        
        var params = {
            module: 'create',
            unit: 'service',
            method: 'getMyStatistics',
            params: {}
        };
        
        $http({
            method: 'POST',
            url: ajaxUrl,
            data: 'params=' + JSON.stringify(params),
            headers : {'Content-Type': 'application/x-www-form-urlencoded'},
            cache: false
        }).success(function(data) { 
            if (typeof(data) == 'object' && data.length !== 0) {
                $scope.itemsStats = itemService.getAll(data);
            }
        });
    };
    
    $scope.itemsLimit = function() {
        return pageSize * pagesShown;
    };
    $scope.hasMoreItemsToShow = function() {
        return pagesShown < ($scope.items.length / pageSize);
    };
    $scope.showMoreItems = function() {
        pagesShown = pagesShown + 1;
    };
    
    $scope.initDoTest = function() {
        var params = {
                module: 'create',
                unit: 'service',
                method: 'getTestDataByTestId',
                params: {testId: $routeParams.testId}
        };

        $http({
            method: 'POST',
            url: ajaxUrl,
            data: 'params=' + JSON.stringify(params),
            headers : {'Content-Type': 'application/x-www-form-urlencoded'},
            cache: false
        }).success(function(data) {  
            $('#test-parent-body').append('<div class="panel-heading"><h3><span class="glyphicon glyphicon-pushpin"></span>&nbsp;' + data.testInfo.title + '</h3></div><br />');
            $('#test-parent-body').append('<div class="question-container-do panel-group" id="test-body"></div>');
            var questions = data.testQuestions;
            for (var question in questions) {
                var questionValue = '<h4><a data-toggle="collapse" data-parent="#test-body" style="cursor:pointer;" data-target="#collapse' + questions[question].id + '"><span class="glyphicon glyphicon-arrow-right"></span>&nbsp;' + questions[question].title + '</a></h4>';
                
                if (typeof(questions[question].title) == undefined) {
                    questionValue = '<h4><a data-toggle="collapse" data-parent="#test-body" style="cursor:pointer;" data-target="#collapse' + questions[question].id + '"><span class="glyphicon glyphicon-paperclip"></span>&nbsp;File Download</a></h4>';
                } 
                $('#test-body').append('<div class="panel panel-default test-question-main" id="question' + questions[question].id + '">' + 
                  '<div class="panel-heading">' + questionValue + '</div>' +
                 '</div>');
                var answers = questions[question].answers;
                
                for (var answer in answers) {
                    var answerValue = answers[answer].title;
                    
                    if (typeof(answers[answer].title) == undefined) {
                        answerValue = '<a>File Download</a>';
                    } 
                    
                    $('#question' + questions[question].id).append('<div class="panel-collapse collapse in test-question" id="collapse' + questions[question].id + '"></div>');
                    $('#collapse' + questions[question].id).append('<div class="panel-body">' + 
                      '<div class="' + questions[question].type + ' ' + questions[question].type + '-primary ' + questions[question].type + '-inline"><input class="form-control do-test-label" type="' + questions[question].type + '" name="qgruop' + questions[question].id + '" id="answer' + answers[answer].id + '" /><label for="answer' + answers[answer].id + '">' + answerValue + '</label></div></div>');
                }
            }
        });
    };
    
    $scope.sendTestResults = function() {
        
        $scope.testGivenCorrectAnswerResults = {};
        
        $('.test-question-main').each(function(qindex) {
            $scope.answerCount = 0;
            
            $(this).children('.test-question').each(function(qindex) {
                
                $(this).children('.panel-body').each(function(pindex) {
                    
                    $(this).children('div').each(function(cindex) {
                        if ($(this).children('input').is(':checked')) {
                            $scope.testGivenCorrectAnswerResults[$(this).children('input').attr('id')] = true;
                            if ($(this).children('input').is('[type=checkbox]')) {
                                $scope.testGivenCorrectAnswerResults[$(this).children('input').attr('id')+'flag'] = true;
                            }
                            $scope.answerCount++;
                        } else {
                            $scope.testGivenCorrectAnswerResults[$(this).children('input').attr('id')] = false;
                        }
                        
                    });
                });
            });
            
            if ($scope.answerCount < 1) {
                bootbox.alert($scope.strings.not_enough_answers_selected, function() {});
                $scope.answerCount = 0;
                $scope.hasError = true;
                return false;
            }
        });
        
        if ($scope.hasError == true) {
            $scope.hasError = false;
            return;
        }
        var params = {
                module: 'create',
                unit: 'service',
                method: 'checkResults',
                params: {
                            results: $scope.testGivenCorrectAnswerResults, 
                            testId: $routeParams.testId
                        }
            };
        
        $http({
            method: 'POST',
            url: ajaxUrl,
            data: 'params=' + JSON.stringify(params),
            headers : {'Content-Type': 'application/x-www-form-urlencoded'},
            cache: false
        }).success(function(data) { 
            console.log(data);
            if (data !== undefined) {
                
                $('#test-parent-body').empty();
                $('#test-parent-body').removeClass('panel');
                $('#test-parent-body').removeClass('panel-primary');
                $('#finishTestBody').remove();
                
                var resultColor = '';
                
                if (data.mark < 50) {
                    resultColor = 'danger';
                } else if (data.mark >= 50 && data.mark < 80) {
                    resultColor = 'warning';
                } else {
                    resultColor = 'success';
                }
                
                $('#test-parent-body').append('<div id="login-error-messages" class="alert alert-' + resultColor + '" role="alert"><p>'+ $scope.strings.achieved +'<b>' + data.mark + ' / ' + data.maxMark + '</b>' + $scope.strings.points + '</p></div>'); 
            }
        });
    }
}]);
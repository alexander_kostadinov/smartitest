'use strict';

var myApp = angular.module('smarti');

myApp.controller('createTestController', ['$scope', '$route', '$routeParams', '$location', '$rootScope', '$sce', '$compile', '$http', '$upload', function($scope, $route, $routeParams, $location, $rootScope, $sce, $compile, $http, $upload) {
    //string initializer
    $scope.strings = strings;
    
    /**
     * Initializes the create test functionalities
     * @author alexander.kostadinov
     */
    $scope.init = function() {
        $('li[name="home-menu-item"]').removeClass('active');
        $('li[name="create-menu-item"]').addClass('active');
        $('li[name="do-menu-item"]').removeClass('active');
        
        $scope.showAddQuestionButton = false;
        $scope.showTestTitleField = false;
        $scope.showCreateTestButton = true;
        $scope.showFinishTestButton = false;
    };

    /**
     * Initializes the create test functionalities after 'create test' button has been clicked
     * @author alexander.kostadinov
     */
    $scope.initTest = function() {
        $scope.showAddQuestionButton = true;
        $scope.showTestTitleField = true;
        $scope.showCreateTestButton = false;
        $scope.showFinishTestButton = true;
        $scope.testContent = [];
        $scope.questionCounter = 0;
        
        $scope.questTextSelected = [];
        $scope.questFileSelected = [];
        
        $scope.answTextSelected = [];
        $scope.answFileSelected = [];
        
        $scope.correctAnswCounter = [];
        $scope.answCounterQuestion = {};
        
        $('.main-cont-create').addClass('panel panel-default');
        $('.main-cont-create-head').addClass('panel-heading');
        $('.main-cont-create-head').append('<p>' + $scope.strings.create_test + '</p>');
    };
    
    /**
     * Adds a question in the DOM
     * @author alexander.kostadinov
     */
    $scope.addQuestion = function() {
        if ($('#addQuestionButton').hasClass('btn-warning')) {
            $('#addQuestionButton').removeClass('btn-warning')
            $('#addQuestionButton').addClass('btn-primary');
        }
        $scope.questionCounter++;
        var q = $scope.questionCounter;

        var html = angular.element('<div class="question form-group" id="question' + q + '"><h4 class="question-title-h">' + $scope.strings.question +'&nbsp;' + q + '</h4><br/><span class="label-left"><div class="radio radio-primary radio-inline"><input class="form-control" type="radio" id="selectQuestMode' + q + 't" name="selectQuestMode' + q + '" ng-model="qvalue' + q + '" value="1" ng-change="changeQuestMode(qvalue' + q + ', ' + q + ')" ng-checked="1"/><label ng-bind-html="strings.text" for="selectQuestMode' + q + 't"></label></div>'
        + '<div class="radio radio-primary radio-inline"><input class="form-control" type="radio" id="selectQuestMode' + q + 'f" name="selectQuestMode' + q + '" ng-model="qvalue' + q + '" value="2" ng-change="changeQuestMode(qvalue' + q + ', ' + q + ')" /><label ng-bind-html="strings.file" for="selectQuestMode' + q + 'f"></label></div></span>'
         +'<input placeholder="' + $scope.strings.question + '..." class="form-control" type="text" ng-model="textinputq' + q + '" ng-if="questTextSelected[' + q + ']" />'
        + '<input ng-if="questFileSelected[' + q + ']" class="form-control input-type-file" type="file" ng-model="fileinputq' + q + '" accept="audio/mp4,audio/mpeg,audio/ogg,audio/webm,video/avi,video/mpeg,video/mp4,video/ogg,video/quicktime,video/webm,video/x-flv,image/gif,image/jpeg,image/png" />'
         +'<button class="btn btn-primary question-answer-add" ng-bind-html="strings.add_answer" ng-click="addAnswer(' + q + ')"></button>'
         +'<button class="btn btn-danger btn-remove-q" title="{{strings.del}}" ng-click="removeElement(\'question' + q + '\')"><span class="glyphicon glyphicon-trash system-icon"></span></button></div>');

         //+'<div style="width: 100px;height:100px; border: 1px dashed blue;" class="drop-box" ng-file-drop="onFileSelect($files)" ng-file-drop-available="dropSupported=true" ng-file-drag-over-class="dragOverClass($event)" ng-file-drag-over-delay="100"  ng-show="questFileSelected[' + q + ']">or drop files <div>here</div></div>');
        
        $compile(html)($scope);
        
        if (q == 1) { 
            $(html).insertAfter($('#addQuestionButton'));
            $('#addQuestionButton').insertAfter($('#question' + (q)));
        } else if (q > 1 && $('#question' + (q)).length > 0) {
            $(html).insertAfter($('#question' + (q - 1)));
            $('#addQuestionButton').insertAfter($('#question' + (q)));
        } else {
            $(html).insertAfter($('#addQuestionButton'));
            $('#addQuestionButton').insertAfter($('#question' + (q)));
        }
        
        $('#question' + q).attr('ransw', 0);
        $scope.correctAnswCounter[q] = 0;
        
        $scope.changeQuestMode(1, q);
        
        $scope.answCounterQuestion['quest'+q] = '';
        $scope.answTextSelected[q] = new Array();
        $scope.answFileSelected[q] = new Array();

    };
    
    /**
     * Changes question mode - text or file
     * @author alexander.kostadinov
     */
    $scope.changeQuestMode = function(choice, question) {
        $('#question' + question).removeClass('has-error');
        
        if (choice == 1) {
            $scope.questTextSelected[question] = true;
            $scope.questFileSelected[question] = false;
        } else {
            $scope.questTextSelected[question] = false;
            $scope.questFileSelected[question] = true;
        }
    };
    
    /**
     * Changes answer mode - text or file
     * @author alexander.kostadinov
     */
    $scope.changeAnswMode = function(choice, answer, question) {
        $('#answer' + answer + 'q' + question).removeClass('has-error');
        if (choice == 1) {
            $scope.answTextSelected[question][answer] = true;
            $scope.answFileSelected[question][answer] = false;
        } else {
            $scope.answTextSelected[question][answer] = false;
            $scope.answFileSelected[question][answer] = true;
        }
    };
    
    /**
     * Adds an answer to a given question (manipulates the DOM)
     * @author alexander.kostadinov
     */
    $scope.addAnswer = function(question) {
        if ($('#question' + question).children('.question-answer-add').hasClass('btn-warning')) {
            $('#question' + question).children('.question-answer-add').removeClass('btn-warning');
            $('#question' + question).children('.question-answer-add').addClass('btn-primary');
        }
        
        $scope.answCounterQuestion['quest'+question]++;
        var a = $scope.answCounterQuestion['quest'+question];
        
        var html = angular.element('<div class="answer form-group" id="answer' + a + 'q'+ question +'"><h5 class="answer-title-h">' + $scope.strings.answer +'&nbsp;' + a + '</h5><br/><span class="label-left"><div class="radio radio-primary radio-inline"><input id="selectAnswMode' + a + 'q'+ question +'t" class="form-control" type="radio" name="selectAnswMode' + a + 'q'+ question +'" ng-model="avalue' + a + 'q'+ question + '" value="1" ng-change="changeAnswMode(avalue' + a  + 'q'+ question + ', ' + a + ', '+ question +')" ng-checked="1" /><label ng-bind-html="strings.text" for="selectAnswMode' + a + 'q'+ question +'t"></label></div>'
        + '<div class="radio radio-primary radio-inline"><input class="form-control" id= "selectAnswMode' + a + 'qf'+ question +'" type="radio" name="selectAnswMode' + a + 'q'+ question +'" ng-model="avalue' + a  + 'q'+ question + '" value="2" ng-change="changeAnswMode(avalue' + a  + 'q'+ question + ', ' + a + ', '+ question +')" /><label ng-bind-html="strings.file" for="selectAnswMode' + a + 'qf'+ question +'"></label></div></span>'
         +'<input placeholder="' + $scope.strings.answer + '..." class="form-control" type="text" ng-model="textinputa' + a + 'q'+ question +'" ng-if="answTextSelected[' + question + '][' + a + ']" />'
        + '<input ng-if="answFileSelected[' + question + '][' + a + ']" class="form-control input-type-file" type="file" ng-model="fileinputa' + a + 'q'+ question +'" accept="audio/mp4,audio/mpeg,audio/ogg,audio/webm,video/avi,video/mpeg,video/mp4,video/ogg,video/quicktime,video/webm,video/x-flv,image/gif,image/jpeg,image/png" /></span>'
         +'<span class="answer-correct"><div class="checkbox checkbox-primary checkbox-inline"><input class="form-control correct-answer" type="checkbox" id="isRightAnswer' + a + 'q'+ question +'" ng-click="changeIsRightAnswer('+ a +','+ question +')" /><label for="isRightAnswer' + a + 'q'+ question +'" ng-bind-html="strings.right_answer"></label></div>' +
         '<button class="btn btn-danger btn-remove ans-rem-but" title="{{strings.del}}" ng-click="removeElement(\'answer' + a + 'q'+ question + '\')"><span class="glyphicon glyphicon-trash system-icon"></span></button></span></div>');
        
        //+'<div style="width: 100px;height:100px; border: 1px dashed blue;" class="drop-box" ng-file-drop="onFileSelect($files)" ng-file-drop-available="dropSupported=true" ng-file-drag-over-class="dragOverClass($event)" ng-file-drag-over-delay="100"  ng-show="answFileSelected[' + question + '][' + a + ']">or drop files <div>here</div></div>');
        $compile(html)($scope);
        
        if (a == 1) { 
            $('#question' + question).append(html);
        } else if (a > 1) {
            $('#question' + question).append(html); 
        }
        
        
        $scope.changeAnswMode(1, a, question);
    };
    
    /**
     * Removes an element from the DOM by given element id
     * @author alexander.kostadinov
     */
    $scope.removeElement = function(id) {
        
        bootbox.dialog({
            message: $scope.strings.delete_warning,
            title: $scope.strings.del,
            buttons: {
                danger: {
                    label: $scope.strings.del,
                    className: 'btn-danger',
                    callback: function() {
                        $( "#" + id).remove();
                    }
                },
                success: {
                    label: $scope.strings.close,
                    className: 'btn-default',
                }
            }
        });
    };
    
    /**
     * Counts how many correct answers are checked for a question
     * @author alexander.kostadinov
     */
    $scope.changeIsRightAnswer = function(answer, question) {
        
        if ($('#isRightAnswer' + answer + 'q' + question).is(':checked')) { 
            $scope.correctAnswCounter[question] += 1;
            
            $('#answer' + answer + 'q'+ question).attr('correct', 1);
            $('#question' + question).attr('ransw', $scope.correctAnswCounter[question]);
            return;
        }
       
        $scope.correctAnswCounter[question] -= 1;
        $('#answer' + answer + 'q'+ question).removeAttr('correct');
        $('#question' + question).attr('ransw', $scope.correctAnswCounter[question]);
    }
    /**
     * Does test submit and validates the test form against empty entries
     * @author alexander.kostadinov
     */    
    $scope.submitTest = function() {
        
        $('#testTitleContainer').removeClass('has-error');
        
        $scope.testObj = {}; 
        
        $scope.testObj.questions = {};
        
        var showCorrectAnswAlert = false;
        var emptyQuestionFieldsCounter = 0;
        
        if ($('#testTitle').val() == '') {
            $('#testTitleContainer').addClass('has-error');
            bootbox.alert($scope.strings.no_test_title, function() {});
            return;
        }
        
        if ($('.question').length == 0) {
            $('#addQuestionButton').removeClass('btn-primary');
            $('#addQuestionButton').addClass('btn-warning');
            bootbox.alert($scope.strings.not_enough_questions, function() {});
            return;
        }
        
        $('.question').each(function(qindex) {
            $(this).removeClass('has-error');
            $scope.testObj.questions[qindex] = {};
            var correctAnswCount = $(this).attr('ransw'); 
            
            if (correctAnswCount < 1) {
                showCorrectAnswAlert = true
            }
            
            if ($(this).children('.answer').length < 2) {
                $(this).children('.question-answer-add').removeClass('btn-primary');
                $(this).children('.question-answer-add').addClass('btn-warning');
                
                $scope.noAnswersEnabled = true;
            } 
            
            if ($(this).children('input').val() == '') {
                $(this).addClass('has-error'); 
                emptyQuestionFieldsCounter++;
            } else {
                $(this).removeClass('has-error'); 
                $scope.testObj.questions[qindex].answers = {};
                if ($(this).children('input').is( "[type=text]" )) {
                    $scope.testObj.questions[qindex].qvalue = $(this).children('input').val();
                } else {
                    var file = $(this).children('input')[0].files[0];
                    var FR =  new FileReader();
                    FR.onload = function(e) {
                        var result = e.target.result; 
//                        var fileObj = {
//                            'name':  file.name.substr(0, file.name.lastIndexOf('.')) || file.name,
//                            'extension': file.name.substr((~-file.name.lastIndexOf('.') >>> 0) + 2),
//                            'fileData': result
//                         } 
                         
                        $scope.testObj.questions[qindex].qvalue = result;
//                        $scope.testObj.questions[qindex].qvalue.value.name = file.name.substr(0, file.name.lastIndexOf('.')) || file.name;
//                        $scope.testObj.questions[qindex].qvalue.value.extension =  file.name.substr((~-file.name.lastIndexOf('.') >>> 0) + 2);
//                        $scope.testObj.questions[qindex].qvalue.value.fileData = result;
                        //$scope.testObj.questions[qindex].qvalue = fileObj;
                    };       
                    FR.readAsDataURL(file);
                }   
            }
            
            $(this).children('.answer').each(function(aindex) {  
                $(this).removeClass('has-error');
                $scope.testObj.questions[qindex].answers[aindex] = {};
                if ($(this).children('input').val() == '' || emptyQuestionFieldsCounter > 0) {
                    $(this).addClass('has-error'); 
                    emptyQuestionFieldsCounter++;
                    
                } else {
                    $(this).removeClass('has-error'); 
                    
                    if ($(this).children('input').is( "[type=text]" )) {
                        
                        var answ = {};
                        
                        if ($(this).is('[correct]')) { 
                            answ.isCorrect = true;
                        } else { 
                            answ.isCorrect  = false 
                        };
                        
                        answ.value = $(this).children('input').val();
                        
                        $scope.testObj.questions[qindex].answers[aindex] = answ;
                        
                    } else {
                        var file = $(this).children('input')[0].files[0];
                        var FR =  new FileReader();
                        FR.onload = function(e) {
                            var result = e.target.result; 
//                            var fileObj = {
//                                'name':  file.name.substr(0, file.name.lastIndexOf('.')) || file.name,
//                                'extension': file.name.substr((~-file.name.lastIndexOf('.') >>> 0) + 2),
//                                'fileData': result
//                             } 
//                             
                            var answ = {};
                            
                            if ($(this).is('[correct]')) { 
                                answ.isCorrect  = true;
                            } else { 
                                answ.isCorrect  = false 
                            };
                            
                            answ.value = result;
//                            answ.value.name = file.name.substr(0, file.name.lastIndexOf('.')) || file.name;
//                            answ.value.extension =  file.name.substr((~-file.name.lastIndexOf('.') >>> 0) + 2);
//                            answ.value.fileData = result;
                            
                            $scope.testObj.questions[qindex].answers[aindex] = answ;
                        };   
                        
                        FR.readAsDataURL(file);
                        
                    }       
                }
            });
        });
        
        if ($scope.noAnswersEnabled == true) {
            $scope.noAnswersEnabled = false;
            bootbox.alert($scope.strings.not_enough_answers, function() {});
            return;
        }
        
        if (showCorrectAnswAlert == true && $scope.noAnswersEnabled !== true) {
            
            bootbox.alert($scope.strings.at_least_one_correct, function() {});
            
            return;
        }
        
        if (emptyQuestionFieldsCounter > 0) {
            
            bootbox.alert($scope.strings.empty_test_fields, function() {});
            
            return;
        }

        $scope.testObj.testTitle = $('#testTitle').val();
        
        console.log($scope.testObj);
        
        var params = {
                module: 'create',
                unit: 'service',
                method: 'getTestData',
                params: {'testObj': $scope.testObj}
        };
        console.log(JSON.stringify(params));
        $http({
            method: 'POST',
            url: ajaxUrl,
            data: 'params=' + JSON.stringify(params),
            headers : {'Content-Type': 'application/x-www-form-urlencoded'},
            cache: false
        }).success(function(data) { 
            if (data !== undefined) {
                switch (data[0]) {
                    case 1: 
                        $('#createTest').empty();
                        $('#createTest').append('<div id="login-error-messages" class="alert alert-success" role="alert"><p>'+ $scope.strings.test_success +'</p></div>'); 
                    break;
                    case 2: 
                        bootbox.alert($scope.strings.test_exists, function() {});
                    break;
                }
            }
        });
    };
}]);
<?php
include 'init.php';
if (! isset($_SESSION['username'])) {
    header('Location: index.php');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html prefix="og:http://ogp.me/ns#" ng-app="smarti" lang="en">
<head>
    <meta property="og:image" content="http://sandroid.info/thesis/Assets/Images/welcome.jpg">
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Smarti Test System</title>
    <link rel="icon" type="image/png" href="Assets/Images/favicon.png">
    <script src="Assets/Library/Angular/angular-file-upload-shim.js"></script>
    <script src="Assets/Library/Angular/angular-file-upload-html5-shim.js"></script>
    <script src="Assets/Library/Angular/angular.js"></script>
    <script src="Assets/Library/Angular/angular-file-upload.js"></script>
    <script src="Assets/Library/Angular/angular-route.js"></script>
    <script src="Assets/Library/Angular/angular-animate.js"></script>
    <script src="Assets/Library/Angular/angular-cookies.js"></script>
    <script src="Assets/Library/Angular/angular-resource.js"></script>
    <script src="Assets/Library/Angular/angular-sanitize.js"></script>
    <script src="Assets/Library/jQuery/jquery-1.11.0.js"></script>
    <script src="Assets/Library/JSON.prune.js"></script>
    <script src="Assets/Library/Bootstrap/js/bootstrap.js"></script>
    <script src="Assets/Library/Bootstrap/js/bootbox.js"></script>
    <script src="Assets/Library/Angular/angular-ui-bootstrap.js"></script>
    <script src="Assets/Library/Angular/breadcrumbs.js"></script>
    <script src="Assets/Scripts/routeProvider.js"></script>
    <link rel="stylesheet" href="Assets/Library/Bootstrap/css/bootstrap.css">
    <link rel="stylesheet"
    	href="Assets/Library/Bootstrap/css/bootstrap-theme.css">
    <link rel="stylesheet" href="Assets/Styles/main.css">
    <link rel="stylesheet" href="Assets/Styles/bootstrap-checkbox-radio.css">
    <!-- Used for clear urls -->
    <!-- <base href="/smartiTest/main.php">  -->
</head>
<body ng-controller="mainController" class="logged-in-body">
	<div class="logged-container" ng-init="initHome()">
		<div id="controls-container">
			<div class="lang-bar">
				<a href="#"><img class="lang" id="bg"
					src="Assets/Images/flag_bg.png" alt="bg" title="{{strings.bg}}" />
				</a> <a href="#"><img class="lang" id="en"
					src="Assets/Images/flag_en.png" alt="en" title="{{strings.en}}" />
				</a> <a href="#"><img class="lang" id="de"
					src="Assets/Images/flag_de.png" alt="de" title="{{strings.de}}" />
				</a>
			</div>
			<div class="dropdown user-dropdown">
				<button type="button" class="btn btn-success" data-toggle="dropdown"
					ng-bind-html="displayUserName"></button>
				<ul class="dropdown-menu user-dr-menu" role="menu"
					aria-labelledby="dLabel">
					<li><a href="#myStats" ng-show="showMyStats"
						ng-bind-html="strings.stats"></a></li>
					<li><a href="" ng-show="showPassChange"
						ng-click="doChangePass('open')" ng-bind-html="strings.change_pass"></a>
					</li>
					<li><a href="" ng-show="showLogout" ng-click="doLogOut()"
						ng-bind-html="strings.logout"></a></li>
				</ul>
			</div>
		</div>
		<ol class="ab-nav breadcrumb">
			<li><a ng-href="#" ng-bind-html="strings.system_heading"
				class="margin-right-xs"></a></li>
			<li
				ng-repeat="breadcrumb in breadcrumbs.get() track by breadcrumb.path"
				ng-class="{ active: $last }"><a ng-if="!$last"
				ng-href="#{{ breadcrumb.path }}" ng-bind="breadcrumb.label"
				class="margin-right-xs"></a> <span ng-if="$last"
				ng-bind="breadcrumb.label"></span>
			</li>
		</ol>
		<h1 ng-bind-html="strings.system_heading"></h1>
		<ul class="nav nav-tabs" role="tablist">
			<li name="home-menu-item"><a href="#home" ng-bind-html="strings.home"
				role="tab" data-toggle="tab"></a></li>
			<li name="create-menu-item"><a href="#createTest"
				ng-bind-html="strings.create_test" ng-show="showCreateTest"
				role="tab" data-toggle="tab"></a></li>
			<li name="do-menu-item"><a href="#doTest"
				ng-bind-html="strings.search_test" ng-show="showDoTest" role="tab"
				data-toggle="tab"></a></li>
		</ul>
		<div class="tab-content" id="mainContainer" ng-view></div>

	</div>
	<div class="col-md-12" id="footer-logged">
		<p id="footerText" ng-bind-html="strings.footer_text"></p>
	</div>
</body>
</html>


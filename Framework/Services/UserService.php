<?php

/**
 * User Service Class
 * @author alexander.kostadinov
 *
 */
class UserService extends ResourceHandler
{
    private $resource;

    private function getResource()
    {
        $this->resource = $_SESSION['reg']->registry['resource'];
    }

    /**
     * Changes User Pass
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function doChangePass($params)
    {
        $this->getResource();


        $modelHandler = $this->resource->initResource('user', 'model');
        $this->resource->execute($modelHandler, 'doChangePass', $params);

        $result = $this->resource->getResponse();

        return $result;
    }

    /**
     * Restores the user password
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function doRestorePass($params)
    {
        $this->getResource();

        $modelHandler = $this->resource->initResource('user', 'model');
        $this->resource->execute($modelHandler, 'doRestorePass', $params);

        $result = $this->resource->getResponse();

        return $result;
    }

    /**
     * Registers a user
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function doRegisterUser($params)
    {
        $this->getResource();

        $modelHandler = $this->resource->initResource('user', 'model');
        $this->resource->execute($modelHandler, 'doRegisterUser', $params);

        $result = $this->resource->getResponse();

        return $result;
    }
}
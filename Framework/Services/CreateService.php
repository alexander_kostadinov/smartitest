<?php

class CreateService extends ResourceHandler
{
    private $resource;

    private function getResource()
    {
        $this->resource = $_SESSION['reg']->registry['resource'];
    }

    public function getTestData($params) {

        $this->getResource();

        $modelHandler = $this->resource->initResource('create', 'model');
        $this->resource->execute($modelHandler, 'getTestData', $params);

        $result = $this->resource->getResponse();

        return $result;
    }

    public function getAllTestsByUserId($params) {

        $this->getResource();

        $modelHandler = $this->resource->initResource('create', 'model');
        $this->resource->execute($modelHandler, 'getAllTestsByUserId', $params);

        $result = $this->resource->getResponse();

        return $result;
    }

    public function getAllTests($params) {

        $this->getResource();

        $modelHandler = $this->resource->initResource('create', 'model');
        $this->resource->execute($modelHandler, 'getAllTests', $params);

        $result = $this->resource->getResponse();

        return $result;
    }

    public function getTestDataByTestId($params) {

        $this->getResource();

        $modelHandler = $this->resource->initResource('create', 'model');
        $this->resource->execute($modelHandler, 'getTestDataByTestId', $params);

        $result = $this->resource->getResponse();

        return $result;
    }

    public function checkResults($params) {

        $this->getResource();

        $modelHandler = $this->resource->initResource('create', 'model');
        $this->resource->execute($modelHandler, 'checkResults', $params);

        $result = $this->resource->getResponse();

        return $result;
    }

    public function getMyStatistics($params) {

        $this->getResource();

        $modelHandler = $this->resource->initResource('create', 'model');
        $this->resource->execute($modelHandler, 'getMyStatistics', $params);

        $result = $this->resource->getResponse();

        return $result;
    }
}
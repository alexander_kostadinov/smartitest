<?php

class ExamService extends ResourceHandler
{
    private $resource;

    private function getResource()
    {
        $this->resource = $_SESSION['reg']->registry['resource'];
    }

    public function testDatabase($params)
    {
        $this->getResource();

        $modelHandler = $this->resource->initResource('exam', 'model');

        $result = $this->resource->execute($modelHandler, 'testDatabase', $params);

        return $result;
    }
}
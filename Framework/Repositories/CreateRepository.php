<?php

class CreateRepository extends ResourceHandler
{
    private $db;

    private function getDb()
    {
        $this->db = $_SESSION['reg']->registry['database'];
    }

    public function insertTestInfo($params)
    {
        $this->getDb();
        $sql = 'INSERT INTO test
                        (title, author_id)
                        VALUES
                        (:title, :author_id)';

        $this->db->query($sql);
        $this->db->bind(':title', $params->testTitle);
        $this->db->bind(':author_id', $params->authorId);
        $this->db->execute();

        return $this->db->lastInsertId();
    }

    public function insertQuestionInfo($params)
    {
        $this->getDb();
        $sql = 'INSERT INTO question
                        (test_id, title, file)
                        VALUES
                        (:test_id, :title, :file)';

        $this->db->query($sql);
        $this->db->bind(':test_id', $params->testId);
        $this->db->bind(':title', $params->title);
        $this->db->bind(':file', $params->file);
        $this->db->execute();

        return $this->db->lastInsertId();
    }

    public function insertAnswerInfo($params)
    {
        $this->getDb();
        $sql = 'INSERT INTO answer
                        (question_id, title, file, is_correct)
                        VALUES
                        (:question_id, :title, :file, :is_correct)';

        $this->db->query($sql);
        $this->db->bind(':question_id', $params->questionId);
        $this->db->bind(':title', $params->title);
        $this->db->bind(':file', $params->file);
        $this->db->bind(':is_correct', $params->isCorrect);
        $this->db->execute();

        return $this->db->lastInsertId();
    }

    public function getLastInsertedTestId($params)
    {
        $this->getDb();
        $sql = 'SELECT id FROM test ORDER BY id DESC LIMIT 0 , 1';
        $this->db->query($sql);
        $this->db->execute();

        return $this->db->single();
    }

    public function getTestByTitle($params)
    {
        $this->getDb();
        $sql = 'SELECT * FROM test AS t WHERE t.title = :title';
        $this->db->query($sql);
        $this->db->bind(':title', $params->testTitle);
        $this->db->execute();
        $result = $this->db->single();

        return $result;
    }

    public function getLastInsertedQuestionId($params)
    {
        $this->getDb();
        $sql = 'SELECT id FROM question ORDER BY id DESC LIMIT 0 , 1';
        $this->db->query($sql);
        $this->db->execute();

        return $this->db->single();
    }

    public function getAllTestsByUserId($params) {

        $this->getDb();
        $sql = 'SELECT * FROM test WHERE author_id = :author_id ORDER BY id DESC';
        $this->db->query($sql);
        $this->db->bind(':author_id', $params->authorId);
        $this->db->execute();

        return $this->db->resultSet();
    }

    public function getAllTests($params) {

        $this->getDb();
        $sql = 'SELECT t.*, u.username FROM test AS t
                        LEFT JOIN user AS u ON (u.id = t.author_id)
                        ORDER BY t.id DESC';
        $this->db->query($sql);
        $this->db->execute();

        return $this->db->resultSet();
    }

    public function getTestById($params) {

        $this->getDb();
        $sql = 'SELECT id, title FROM test WHERE id = :test_id';
        $this->db->query($sql);
        $this->db->bind(':test_id', $params->testId);
        $this->db->execute();

        return $this->db->single();
    }

    public function getQuestionsByTestId($params) {

        $this->getDb();
        $sql = 'SELECT id, title, file FROM question WHERE test_id = :test_id';
        $this->db->query($sql);
        $this->db->bind(':test_id', $params->testId);
        $this->db->execute();

        return $this->db->resultSet();
    }

    public function getAnswersByQuestionId($params) {

        $this->getDb();
        $sql = 'SELECT id, title, file, is_correct FROM answer WHERE question_id = :question_id';
        $this->db->query($sql);
        $this->db->bind(':question_id', $params->questionId);
        $this->db->execute();

        return $this->db->resultSet();
    }

    public function getAnswersByTestId($params) {

        $this->getDb();
        $sql = 'SELECT a.* FROM test AS t
                        LEFT JOIN question AS q ON (q.test_id = t.id)
                        LEFT JOIN answer AS a ON (a.question_id = q.id)
                        WHERE t.id = :test_id';
        $this->db->query($sql);
        $this->db->bind(':test_id', $params->testId);
        $this->db->execute();

        return $this->db->resultSet();
    }

    public function getUserTestResults($params) {

        $this->getDb();
        $sql = 'SELECT * FROM user_test_result WHERE user_id = :user_id AND test_id = :test_id';
        $this->db->query($sql);
        $this->db->bind(':user_id', $params->userId);
        $this->db->bind(':test_id', $params->testId);
        $this->db->execute();

        return $this->db->single();
    }

    public function insertUserTestScore($params) {

        $this->getDb();
        $sql = 'INSERT INTO user_test_result (user_id, test_id, last_score, best_score)
                        VALUES (:user_id, :test_id, :last_score, :best_score)';
        $this->db->query($sql);
        $this->db->bind(':user_id', $params->userId);
        $this->db->bind(':test_id', $params->testId);
        $this->db->bind(':last_score', $params->lastScore);
        $this->db->bind(':best_score', $params->bestScore);
        $this->db->execute();

        return $this->db->single();
    }

    public function updateUserTestScore($params) {

        $this->getDb();
        $sql = 'UPDATE user_test_result SET last_score = :last_score, best_score = :best_score
                        WHERE user_id = :user_id AND test_id = :test_id';
        $this->db->query($sql);
        $this->db->bind(':user_id', $params->userId);
        $this->db->bind(':test_id', $params->testId);
        $this->db->bind(':last_score', $params->lastScore);
        $this->db->bind(':best_score', $params->bestScore);
        $this->db->execute();

        return $this->db->lastInsertId();
    }

    public function getMyStatistics($params) {
        $this->getDb();
        $sql = 'SELECT utr.*, t.title, u.username from user_test_result AS utr
                        LEFT JOIN test AS t ON (utr.test_id = t.id)
                        LEFT JOIN user AS u ON (u.id = t.author_id)
                        WHERE utr.user_id = :user_id';
        $this->db->query($sql);
        $this->db->bind(':user_id', $params->userId);
        $this->db->execute();

        return $this->db->resultSet();
    }
}
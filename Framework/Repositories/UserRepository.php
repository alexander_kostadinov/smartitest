<?php

/**
 * User Repository Class
 * @author alexander.kostadinov
 *
 */
class UserRepository extends ResourceHandler
{
    private $db;

    private function getDb()
    {
        $this->db = $_SESSION['reg']->registry['database'];
    }

    /**
     * Changes the user password
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function changeUserPass($params)
    {
        $this->getDb();
        $sql = 'UPDATE USER
                        SET password = :password
                        WHERE id = :user_id';

        $this->db->query($sql);
        $this->db->bind(':password', md5($params->password));
        $this->db->bind(':user_id', $params->userId);
        $this->db->execute();

        return $this->db->lastInsertId();
    }

    /**
     * Registers a user
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function doRegisterUser($params)
    {
        $this->getDb();
        $sql = 'INSERT INTO USER (username,password,email,reg_date,is_locked,role)
                        VALUES(
                        :username,
                        :password,
                        :email,
                        NOW(),
                        :is_locked,
                        :role)';

        $this->db->query($sql);
        $this->db->bind(':username', $params->username);
        $this->db->bind(':password', md5($params->password));
        $this->db->bind(':email', $params->email);
        $this->db->bind(':is_locked', 0);
        $this->db->bind(':role', $params->role);
        $this->db->execute();

        return $this->db->lastInsertId();
    }
}
<?php

/**
 * Login Repository Class
 * @author alexander.kostadinov
 *
 */
class LoginRepository extends ResourceHandler
{
    private $db;

    private function getDb()
    {
        $this->db = $_SESSION['reg']->registry['database'];
    }

    /**
     * Gets user info
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function getLoginUserInfo($params)
    {
        $this->getDb();
        $sql = 'SELECT * FROM USER AS u
                        WHERE u.username = :username';

        $this->db->query($sql);
        $this->db->bind(':username', $params->username);
        $this->db->execute();
        $result = $this->db->single();

        return $result;
    }

    /**
     * Gets user info by email
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function getLoginUserInfoByEmail($params)
    {
        $this->getDb();
        $sql = 'SELECT * FROM USER AS u
                        WHERE u.email = :email';

        $this->db->query($sql);
        $this->db->bind(':email', $params->email);
        $this->db->execute();
        $result = $this->db->single();

        return $result;
    }

    /**
     * Inserts a login attempt
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function insertUserLoginAttempt($params)
    {
        $this->getDb();
        $sql = 'INSERT INTO USER_LOGIN_ATTEMPT
                        (user_id)
                        VALUES
                        (:user_id)';

        $this->db->query($sql);
        $this->db->bind(':user_id', $params->userId);
        $this->db->execute();

        return $this->db->lastInsertId();
    }

    /**
     * Gets number of login attempts
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function getNumberOfLoginAttempts($params)
    {
        $this->getDb();
        $sql = 'SELECT COUNT(*) as count FROM USER_LOGIN_ATTEMPT AS u
                        WHERE u.user_id = :user_id';

        $this->db->query($sql);
        $this->db->bind(':user_id', $params->userId);
        //$this->db->bind(':time', $params->validAttemptsTime);
        $this->db->execute();
        $result = $this->db->single();

        return $result;
    }

    public function getLastLoginAttempt($params)
    {
        $this->getDb();
        $sql = 'SELECT MAX(login_attempt_time) AS time FROM USER_LOGIN_ATTEMPT AS u
                        WHERE u.user_id = :user_id';

        $this->db->query($sql);
        $this->db->bind(':user_id', $params->userId);
        $this->db->execute();
        $result = $this->db->single();

        return $result;
    }

    /**
     * Updates user record to be locked
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function lockUser($params)
    {
        $this->getDb();
        $sql = 'UPDATE USER u
                        SET u.is_locked = 1
                        WHERE u.id = :user_id';

        $this->db->query($sql);
        $this->db->bind(':user_id', $params->userId);
        $this->db->execute();

        return $this->db->lastInsertId();
    }

    /**
     * Updates user record to be unlocked
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function unlockUser($params)
    {
        $this->getDb();
        $sql = 'UPDATE USER u
                        SET u.is_locked = 0
                        WHERE u.id = :user_id';

        $this->db->query($sql);
        $this->db->bind(':user_id', $params->userId);
        $this->db->execute();

        return $this->db->lastInsertId();
    }

    /**
     * Clears the log attempts for a user
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function clearLoginAttempts($params)
    {
        $this->getDb();
        $sql = 'DELETE FROM USER_LOGIN_ATTEMPT
                        WHERE user_id = :user_id';

        $this->db->query($sql);
        $this->db->bind(':user_id', $params->userId);
        $this->db->execute();

        return $this->db->lastInsertId();
    }
}
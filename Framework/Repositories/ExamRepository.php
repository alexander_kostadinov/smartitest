<?php

class ExamRepository extends ResourceHandler
{
    private $db;

    private function getDb()
    {
        $this->db = $_SESSION['reg']->registry['database'];
    }

    public function testDatabase($params)
    {
        $this->getDb();
        $sql = 'INSERT INTO TEST
                        (name, age)
                        VALUES
                        (:name, :age)';

        $this->db->query($sql);
        $this->db->bind(':name', $params['name']);
        $this->db->bind(':age', $params['age']);
        $this->db->execute();

        return $this->db->lastInsertId();
    }
}
<?php

/**
 * User Model Class
 * @author alexander.kostadinov
 *
 */
class UserModel extends ResourceHandler
{
    //change pass error constants
    const CHANGE_PASS_EMPTY_OLD = 1;
    const CHANGE_PASS_EMPTY_NEW = 2;
    const CHANGE_PASS_EMPTY_NEW_AGAIN = 3;
    const CHANGE_PASS_NOT_MATCH = 4;
    const CHANGE_PASS_NEW_PATTERN_FAIL = 5;
    const CHANGE_PASS_OLD_PASS_WRONG = 6;

    //restore pass error constants
    const RESTORE_PASS_EMPTY_EMAIL = 1;
    const RESTORE_PASS_WRONG_EMAIL_PATTERN = 2;
    const RESTORE_PASS_EMPTY_USERNAME = 3;
    const RESTORE_PASS_WRONG_USERNAME = 4;

    //register user error constants
    const REG_EMPTY_USERNAME = 1;
    const REG_EMPTY_PASSWORD = 2;
    const REG_EMPTY_EMAIL = 3;
    const REG_WRONG_PASS_PATTERN = 4;
    const REG_WRONG_EMAIL_PATTERN = 5;
    const REG_USER_EXISTS= 6;
    const REG_EMAIL_EXISTS= 7;

    //necessary resources for every class
    private function getResource()
    {
        $this->resource = $_SESSION['reg']->registry['resource'];
    }

    private function getDb()
    {
        $this->db = $_SESSION['reg']->registry['database'];
    }

    /**
     * Restores the user password
     * @param object $params
     * @return boolean
     * @author alexander.kostadinov
     */
    private function sendPassEmail($params)
    {
        if (! empty($params->email) && ! empty($params->username)) {
            $newPass = $this->generateRandomPass();
            $from = 'Smarti Test System';
            $subject = 'New Password';
            $message = 'Hello, ' . $params->username . ', your new password is ' . $newPass . '. Please change it after you log in.';
            // message lines should not exceed 70 characters (PHP rule), so wrap it
            $message = wordwrap($message, 70);
            // send mail
            mail($params->email, $subject, $message,'From: $from\n');

            return $newPass;
        }
    }

    /**
     * Generates a random password
     * @return string
     * @author alexander.kostadinov
     */
    private function generateRandomPass() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array();
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    /**
     * Changes the user password
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function doChangePass($params)
    {
        $errors = array();

        if (empty($params->oldPass)) {
            $errors[self::CHANGE_PASS_EMPTY_OLD] = self::CHANGE_PASS_EMPTY_OLD;
        }

        if (empty($params->newPass)) {
            $errors[self::CHANGE_PASS_EMPTY_NEW] = self::CHANGE_PASS_EMPTY_NEW;
        }

        if (empty($params->newPassAgain)) {
            $errors[self::CHANGE_PASS_EMPTY_NEW_AGAIN] = self::CHANGE_PASS_EMPTY_NEW_AGAIN;
        }

        if (! empty($params->oldPass)) {
            $this->getResource();
            $this->getDb();

            $repoHandler = $this->resource->initResource('login', 'repository');

            //get user info
            $repoParams = (object) array('username' => $_SESSION['username']);
            $this->resource->execute($repoHandler, 'getLoginUserInfo', $repoParams);
            $userInfo = $this->resource->getResponse();

            if (md5($params->oldPass) != $userInfo['password']) {
                $errors[self::CHANGE_PASS_OLD_PASS_WRONG] = self::CHANGE_PASS_OLD_PASS_WRONG;
                $errors['test'] = $userInfo['password'];
            }
        }

        if (! empty($params->newPass) && ! empty($params->newPassAgain) && $params->newPass != $params->newPassAgain) {
            $errors[self::CHANGE_PASS_NOT_MATCH] = self::CHANGE_PASS_NOT_MATCH;
        }

        $pattern = '/^(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/';
        if (! isset($errors[self::CHANGE_PASS_NEW_PATTERN_FAIL]) && ! empty($params->newPass) && ! preg_match($pattern, $params->newPass)) {
            $errors[self::CHANGE_PASS_NEW_PATTERN_FAIL] = self::CHANGE_PASS_NEW_PATTERN_FAIL;
        }

        if (! isset($errors[self::CHANGE_PASS_NEW_PATTERN_FAIL]) && ! empty($params->newPassAgain) && ! preg_match($pattern, $params->newPassAgain)) {
            $errors[self::CHANGE_PASS_NEW_PATTERN_FAIL] = self::CHANGE_PASS_NEW_PATTERN_FAIL;
        }

        if (! empty($errors)) {
            return $errors;
        }

        $repoHandler = $this->resource->initResource('user', 'repository');

        //update user pass
        $repoParams = (object) array(
                'userId' => $userInfo['id'],
                'password' => $params->newPass,
        );

        $this->db->beginTransaction();
        try {
            $this->resource->execute($repoHandler, 'changeUserPass', $repoParams);
            $this->db->commitTransaction();
        } catch (Exception $e) {
            $this->db->rollBack();
            throw new Exception($e->getMessage());
        }

        return true;
    }

    /**
     * Restores the user password
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function doRestorePass($params) {
        $errors = array();

        if (empty($params->email)) {
            $errors[self::RESTORE_PASS_EMPTY_EMAIL] = self::RESTORE_PASS_EMPTY_EMAIL;
        }

        if (empty($params->username)) {
            $errors[self::RESTORE_PASS_EMPTY_USERNAME] = self::RESTORE_PASS_EMPTY_USERNAME;
        }

        if (! empty($errors)) {
            return $errors;
        }

        $pattern = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/';

        if (! empty($params->email) && ! preg_match($pattern, $params->email)) {
            $errors[self::RESTORE_PASS_WRONG_EMAIL_PATTERN] = self::RESTORE_PASS_WRONG_EMAIL_PATTERN;
        }

        if (! empty($errors)) {
            return $errors;
        }

        if (! empty($params->email) && ! empty($params->username)) {
            $this->getResource();
            $this->getDb();

            $repoHandler = $this->resource->initResource('login', 'repository');

            //get user info
            $repoParams = (object) array('username' => $params->username);
            $this->resource->execute($repoHandler, 'getLoginUserInfo', $repoParams);
            $userInfo = $this->resource->getResponse();

            if ($params->email == $userInfo['email']) {
                $generatedPass = $this->sendPassEmail($params);
                $repoHandler = $this->resource->initResource('user', 'repository');
                $repoParams = (object) array(
                        'userId' => $userInfo['id'],
                        'password' => $generatedPass,
                );

                $this->db->beginTransaction();
                try {
                    $this->resource->execute($repoHandler, 'changeUserPass', $repoParams);
                    $this->db->commitTransaction();
                } catch (Exception $e) {
                    $this->db->rollBack();
                    throw new Exception($e->getMessage());
                }

                return true;
            }

            return array(self::RESTORE_PASS_WRONG_USERNAME => self::RESTORE_PASS_WRONG_USERNAME);
        }
    }

    /**
     * Registers a user
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function doRegisterUser($params) {
        $errors = array();

        if (empty($params->username)) {
            $errors[self::REG_EMPTY_USERNAME] = self::REG_EMPTY_USERNAME;
        }

        if (empty($params->password)) {
            $errors[self::REG_EMPTY_PASSWORD] = self::REG_EMPTY_PASSWORD;
        }

        if (empty($params->email)) {
            $errors[self::REG_EMPTY_EMAIL] = self::REG_EMPTY_EMAIL;
        }

        if (! empty($errors)) {
            return $errors;
        }

        $pattern = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/';

        if (! empty($params->email) && ! preg_match($pattern, $params->email)) {
            $errors[self::REG_WRONG_EMAIL_PATTERN] = self::REG_WRONG_EMAIL_PATTERN;
        }

        $pattern = '/^(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/';

        if (! empty($params->password) && ! preg_match($pattern, $params->password)) {
            $errors[self::REG_WRONG_PASS_PATTERN] = self::REG_WRONG_PASS_PATTERN;
        }

        $this->getResource();
        $this->getDb();

        $repoHandler = $this->resource->initResource('login', 'repository');
        $repoParams = (object) array('username' => $params->username);
        $this->resource->execute($repoHandler, 'getLoginUserInfo', $repoParams);
        $userInfo = $this->resource->getResponse();

        if ($userInfo) {
            $errors[self::REG_USER_EXISTS] = self::REG_USER_EXISTS;
        }

        $repoHandler = $this->resource->initResource('login', 'repository');
        $repoParams = (object) array('email' => $params->email);
        $this->resource->execute($repoHandler, 'getLoginUserInfoByEmail', $repoParams);
        $userInfo = $this->resource->getResponse();

        if ($userInfo) {
            $errors[self::REG_EMAIL_EXISTS] = self::REG_EMAIL_EXISTS;
        }

        if (! empty($errors)) {
            return $errors;
        }

        if (! empty($params->email) && ! empty($params->username) && ! empty($params->password) && isset($params->role)) {
            $repoHandler = $this->resource->initResource('user', 'repository');

            $repoParams = (object) array(
                    'username' => $params->username,
                    'password' => $params->password,
                    'email' => $params->email,
                    'role' => is_numeric((int) $params->role) ? $params->role : 0,
            );

            $this->db->beginTransaction();
            try {
                $this->resource->execute($repoHandler, 'doRegisterUser', $repoParams);
                $this->db->commitTransaction();
            } catch (Exception $e) {
                $this->db->rollBack();
                throw new Exception($e->getMessage());
            }

            return true;
        }

        return false;
    }
}
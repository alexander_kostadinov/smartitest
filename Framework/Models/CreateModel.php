<?php

class CreateModel extends ResourceHandler
{
    const CREATE_TEST_SUCCESS = 1;
    const CREATE_TEST_ALERADY_EXISTS = 2;
    const NO_OWN_TESTS = 3;

    //necessary resources for every class
    private $resource;
    private $db;

    private function getResource()
    {
        $this->resource = $_SESSION['reg']->registry['resource'];
    }

    private function getDb()
    {
        $this->db = $_SESSION['reg']->registry['database'];
    }

    private function calculatePointWorth($pointsCount) {

        if ($pointsCount != 0) {
            return round((100 / $pointsCount), 2);
        }

        return false;
    }
    public function getTestData($params) {

        $this->getResource();
        $this->getDb();

        $repoHandler = $this->resource->initResource('login', 'repository');
        $repoParams = (object) array('username' => $_SESSION['username']);
        $this->resource->execute($repoHandler, 'getLoginUserInfo', $repoParams);
        $userInfo = $this->resource->getResponse();

        $repoHandler = $this->resource->initResource('create', 'repository');

        if (! empty($params->testObj->testTitle)) {

            $this->resource->execute($repoHandler, 'getTestByTitle', (object) array('testTitle' => $params->testObj->testTitle));
            $testData = $this->resource->getResponse();

            if (! empty($testData)) {
                return array(self::CREATE_TEST_ALERADY_EXISTS);
            }
        }

        try {

            $this->db->beginTransaction();

            if (! empty($userInfo)) {
                $repoParamsTest = (object) array(
                        'testTitle' => $params->testObj->testTitle,
                        'authorId' =>  $userInfo['id'],
                );
            }

            $this->resource->execute($repoHandler, 'insertTestInfo', $repoParamsTest);

            $this->resource->execute($repoHandler, 'getLastInsertedTestId', (object) array());
            $testInfo = $this->resource->getResponse();

            if (! empty($params->testObj->questions) && ! empty($testInfo)) {

                foreach ((array) $params->testObj->questions as $question) {
                    $repoParamsQuestion = (object) array(
                            'testId' => $testInfo['id'],
                    );

                    if (! empty($question->qvalue) && is_string($question->qvalue)) {
                        $repoParamsQuestion->title =  $question->qvalue;
                        $repoParamsQuestion->file =  null;
                    } else {
                        $repoParamsQuestion->title =  null;
                        $repoParamsQuestion->file = json_encode ($question->qvalue);
                    }

                    $this->resource->execute($repoHandler, 'insertQuestionInfo', $repoParamsQuestion);

                    $this->resource->execute($repoHandler, 'getLastInsertedQuestionId', (object) array());
                    $questionInfo = $this->resource->getResponse();

                    if (! empty($question->answers) && ! empty($questionInfo)) {

                        foreach ((array) $question->answers as $answer) {

                            $repoParamsAnswer = (object) array(
                                    'questionId' => $questionInfo['id'],
                                    'isCorrect' => (int) $answer->isCorrect,
                            );

                            if (! empty($answer->value) && is_string($answer->value)) {
                                $repoParamsAnswer->title = $answer->value;
                                $repoParamsAnswer->file = null;
                            } else {
                                $repoParamsAnswer->title = null;
                                $repoParamsAnswer->file = json_encode( $answer->value);
                            }

                            $this->resource->execute($repoHandler, 'insertAnswerInfo', $repoParamsAnswer);
                        }
                    }
                }
            }

            $this->db->commitTransaction();
        } catch (Exception $e) {
            $this->db->rollBack();
            throw new Exception($e->getMessage());
        }

        return array(self::CREATE_TEST_SUCCESS);
    }

    public function getAllTestsByUserId($params) {

        $this->getResource();
        $this->getDb();

        $repoHandler = $this->resource->initResource('login', 'repository');
        $repoParams = (object) array('username' => $_SESSION['username']);
        $this->resource->execute($repoHandler, 'getLoginUserInfo', $repoParams);
        $userInfo = $this->resource->getResponse();

        $repoHandler = $this->resource->initResource('create', 'repository');
        $repoParams= (object) array(
                'authorId' =>  $userInfo['id'],
        );

        $this->resource->execute($repoHandler, 'getAllTestsByUserId', $repoParams);
        $result = $this->resource->getResponse();

        if (! empty($result)) {
            return $result;
        }

        return self::NO_OWN_TESTS;
    }

    public function getAllTests($params) {

        $this->getResource();
        $this->getDb();

        $repoHandler = $this->resource->initResource('create', 'repository');
        $this->resource->execute($repoHandler, 'getAllTests', (object) array());

        return $this->resource->getResponse();
    }

    public function getTestDataByTestId($params) {

        $this->getResource();
        $this->getDb();

        $repoHandler = $this->resource->initResource('create', 'repository');

        $this->resource->execute($repoHandler, 'getTestById', (object) array('testId' => $params->testId));
        $testInfo = $this->resource->getResponse();

        $this->resource->execute($repoHandler, 'getQuestionsByTestId', (object) array('testId' => $params->testId));
        $testQuestions = $this->resource->getResponse();

        foreach ($testQuestions as $qkey => $question) {
            $this->resource->execute($repoHandler, 'getAnswersByQuestionId', (object) array('questionId' => $question['id']));
            $questionAnswers = $this->resource->getResponse();
            $correctAnswerCount = 0;

            foreach ($questionAnswers as $akey => $answer) {
                if ((int) $answer['is_correct']) {
                    $correctAnswerCount++;
                }

                if (empty($answer['file'])) {
                    unset($questionAnswers[$akey]['file']);
                } else {
                    unset($questionAnswers[$akey]['title']);
                }

            }

            if (empty($question['file'])) {
                unset($testQuestions[$qkey]['file']);
            } else {
                unset($testQuestions[$qkey]['title']);
            }

            $testQuestions[$qkey]['type'] = 'radio';

            if ($correctAnswerCount > 1) {
                $testQuestions[$qkey]['type'] = 'checkbox';
            }

            $testQuestions[$qkey]['answers'] = $questionAnswers;
        }
         
        $result = array(
                'testInfo' => $testInfo,
                'testQuestions' => $testQuestions
        );

        return $result;
    }

    public function checkResults($params) {

        $this->getResource();
        $this->getDb();

        $repoHandler = $this->resource->initResource('login', 'repository');
        $repoParams = (object) array('username' => $_SESSION['username']);
        $this->resource->execute($repoHandler, 'getLoginUserInfo', $repoParams);
        $userInfo = $this->resource->getResponse();

        $repoHandler = $this->resource->initResource('create', 'repository');
        $this->resource->execute($repoHandler, 'getAnswersByTestId', (object) array('testId' => $params->testId));

        $dbAnswers = $this->resource->getResponse();
        $userAnswers = $params->results;
        $dbCorrectQuestionCounter = 0;
        $earnedPoints = 0;

        foreach ($dbAnswers as $aKey => $answer) {
            if ($answer['is_correct']) {
                $dbCorrectQuestionCounter++;
            }

            $objKey = 'answer' . $answer['id'];
            $objFlagKey = 'answer' . $answer['id'] . 'flag';


            if ((boolean) $answer['is_correct'] == true && $userAnswers->$objKey == true) {
                $earnedPoints++;
            }

            if ((boolean) $answer['is_correct'] == false && $userAnswers->$objKey == true && isset($userAnswers->$objFlagKey) && $userAnswers->$objFlagKey == true && $earnedPoints > 0) {
                $earnedPoints--;
            }
        }

        $onePointPercentageWorth = $this->calculatePointWorth($dbCorrectQuestionCounter);

        $mark = $earnedPoints * $onePointPercentageWorth;
        $maxMark = $dbCorrectQuestionCounter * $onePointPercentageWorth;

        $repoParams = (object) array(
                'userId' => $userInfo['id'],
                'testId' => $params->testId,
        );

        $this->resource->execute($repoHandler, 'getUserTestResults', $repoParams);
        $userTestPreviousResults = $this->resource->getResponse();

        $repoParams = (object) array(
                'userId' => $userInfo['id'],
                'testId' => $params->testId,
                'lastScore' => $mark,
                'bestScore' => $mark,
        );

        $result = array (
                'mark' => $mark,
                'maxMark' => $maxMark,
        );

        if (! $userTestPreviousResults) {

            $this->resource->execute($repoHandler, 'insertUserTestScore', $repoParams);
            return $result;
        }

        if ($userTestPreviousResults['best_score'] > $mark) {

            $repoParams->bestScore = $userTestPreviousResults['best_score'];
        }

        $this->resource->execute($repoHandler, 'updateUserTestScore', $repoParams);

        return $result;
    }

    public function getMyStatistics($params) {

        $this->getResource();
        $this->getDb();

        $repoHandler = $this->resource->initResource('login', 'repository');
        $repoParams = (object) array('username' => $_SESSION['username']);
        $this->resource->execute($repoHandler, 'getLoginUserInfo', $repoParams);
        $userInfo = $this->resource->getResponse();

        $repoHandler = $this->resource->initResource('create', 'repository');
        $repoParams = (object) array('userId' => $userInfo['id']);
        $this->resource->execute($repoHandler, 'getMyStatistics', $repoParams);

        $result = $this->resource->getResponse();

        if (! empty($result)) {
            return $result;
        }

        return self::NO_OWN_TESTS;
    }
}
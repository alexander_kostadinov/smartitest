<?php

/**
 * Login Model Class
 * @author alexander.kostadinov
 *
 */
class LoginModel extends ResourceHandler
{
    //login error constants
    const EMPTY_USERNAME = 1;
    const EMPTY_PASSWORD = 2;
    const WRONG_USERNAME = 3;
    const WRONG_PASSWORD = 4;
    const ACCOUNT_LOCKED = 5;
    const ACCOUNT_UNLOCKED = 6;

    const ACCOUNT_LOCKED_DB = 1; //boolean field in the database, table users
    const ALLOWED_USER_LOGIN_ATTEMPTS = 5; //number of attempts
    const USER_LOGIN_ATTEMPT_CHECK_INTERVAL = 300; //seconds

    //necessary resources for every class
    private $resource;
    private $db;

    private function getResource()
    {
        $this->resource = $_SESSION['reg']->registry['resource'];
    }

    private function getDb()
    {
        $this->db = $_SESSION['reg']->registry['database'];
    }

    /**
     * Checks for empty login parameters
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    private function checkForEmptyLoginParameters($params) {
        $errors = array();

        if (empty($params->username)) {
            $errors[self::EMPTY_USERNAME] = self::EMPTY_USERNAME;
        }

        if (empty($params->password)) {
            $errors[self::EMPTY_PASSWORD] = self::EMPTY_PASSWORD;
        }

        return $errors;
    }

    /**
     * Checks for too many login attempts - 5 set as a constant above
     * @param object $params
     * @return boolean
     * @author alexander.kostadinov
     */
    private function checkForManyLoginAttempts($userId) {
        $repoHandler = $this->resource->initResource('login', 'repository');
        $repoParams = (object) array(
                'userId' => $userId
        );

        $this->resource->execute($repoHandler, 'getNumberOfLoginAttempts', $repoParams);
        $userAttemptsCount = $this->resource->getResponse();

        // If there have been more than 5 failed logins
        if ($userAttemptsCount['count'] >= self::ALLOWED_USER_LOGIN_ATTEMPTS) {
            return false;
        } else {
            return true;
        }
    }

    //TODO: try to optimize, 1 or 2 operations can be removed
    /**
    * Processes the login flow of events
    * @param object $params
    * @return array with errors|boolean
    * @author alexander.kostadinov
    */
    public function doLogin($params)
    {
        //check for empty credentials and return of errors
        $emptyCredentialsCheck = $this->checkForEmptyLoginParameters($params);

        if (! empty($emptyCredentialsCheck)) {

            return $emptyCredentialsCheck;
        }

        //set of resource and db getter and initialization of login repository (used for the whole method)
        $this->getResource();
        $this->getDb();

        $repoHandler = $this->resource->initResource('login', 'repository');

        //get user info
        $this->resource->execute($repoHandler, 'getLoginUserInfo', $params);
        $userInfo = $this->resource->getResponse();

        //validation
        if ($userInfo['username'] != $params->username) {
            return array(self::WRONG_USERNAME => self::WRONG_USERNAME);
        }

        //if name is correct, pass is incorrect and account is unlocked
        if ($userInfo['username'] == $params->username && $userInfo['password'] != md5($params->password)
                        && (int) $userInfo['is_locked'] != self::ACCOUNT_LOCKED_DB) {
            //insert that user has attempted to log in (only 5 insertions allowed)
            $this->db->beginTransaction();
            try {
                $this->resource->execute($repoHandler, 'insertUserLoginAttempt', (object) array('userId' => (int) $userInfo['id']));
                $this->db->commitTransaction();
            } catch (Exception $e) {
                $this->db->rollBack();
                throw new Exception($e->getMessage());
            }

            //if attempts are more than 5, lock user and return that account is locked
            if (! $this->checkForManyLoginAttempts((int) $userInfo['id'])) {
                $this->db->beginTransaction();
                try {
                    $this->resource->execute($repoHandler, 'lockUser', (object) array('userId' => (int) $userInfo['id']));
                    $this->db->commitTransaction();
                } catch (Exception $e) {
                    $this->db->rollBack();
                    throw new Exception($e->getMessage());
                }
                return array(self::ACCOUNT_LOCKED => self::ACCOUNT_LOCKED);
            }

            //else return that he/she has entered a wrong password
            return array(self::WRONG_PASSWORD => self::WRONG_PASSWORD);
        }

        //get the last login attempt and calculate if it has happened in time less than the constant.
        //If so, prevent from going into thw if- above on the next login by unlocking the user & clearing the attempts
        $this->resource->execute($repoHandler, 'getLastLoginAttempt', (object) array('userId' => (int) $userInfo['id']));
        $lastLoginTime = $this->resource->getResponse();
        $timeInterval = ! empty($lastLoginTime['time']) ? time() - strtotime($lastLoginTime['time']) : null;

        if (! empty($timeInterval) && $timeInterval > self::USER_LOGIN_ATTEMPT_CHECK_INTERVAL) {
            $this->db->beginTransaction();
            try {
                $this->resource->execute($repoHandler, 'unlockUser', (object) array('userId' => (int) $userInfo['id']));
                $this->resource->execute($repoHandler, 'clearLoginAttempts', (object) array('userId' => (int) $userInfo['id']));
                $this->db->commitTransaction();
            } catch (Exception $e) {
                $this->db->rollBack();
                throw new Exception($e->getMessage());
            }
        }

        //get user data again, to ensure the lock/unlock state has/hasn't changed
        $this->resource->execute($repoHandler, 'getLoginUserInfo', $params);
        $userInfo = $this->resource->getResponse();

        //if is still locked - return false
        if ((int) $userInfo['is_locked'] == self::ACCOUNT_LOCKED_DB) {
            return false;
        }

        //if unlocked, but wrong pass is inserted again, alert that
        if ($userInfo['username'] == $params->username && $userInfo['password'] != md5($params->password)) {
            return array(self::ACCOUNT_UNLOCKED=> self::ACCOUNT_UNLOCKED);
        }

        //if all has passed well, set the user credentials in the state, clear attempts (if any, no much memory used)
        //and return confirm message
        $_SESSION['reg']->state['user'] = $userInfo['username'];
        $_SESSION['reg']->state['role'] = (int) $userInfo['role'];
        //$this->registry->state['user'] = $userInfo['username'];

        $this->db->beginTransaction();
        try {
            $this->resource->execute($repoHandler, 'clearLoginAttempts', (object) array('userId' => (int) $userInfo['id']));
            $this->db->commitTransaction();
        } catch (Exception $e) {
            $this->db->rollBack();
            throw new Exception($e->getMessage());
        }

        return array(
                'username' => $_SESSION['reg']->state['user'],
                'role' => $_SESSION['reg']->state['role'],
        );
    }
}
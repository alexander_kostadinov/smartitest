<?php

class ExamModel extends ResourceHandler
{
    private $resource;
    private $db;

    private function getResource()
    {
        $this->resource = $_SESSION['reg']->registry['resource'];
    }

    private function getDb()
    {
        $this->db = $_SESSION['reg']->registry['database'];
    }

    public function testDatabase($params)
    {
        $this->getResource();
        $this->getDb();

        $this->db->beginTransaction();
        try {
            $repolHandler = $this->resource->initResource('exam', 'repository');
            $this->resource->execute($repolHandler, 'testDatabase', $params);
            $result = $repolHandler->getResponse();
            $this->db->commitTransaction();
        } catch (Exception $e) {
            $this->db->rollBack();
            throw new Exception($e->getMessage());
        }

        return $result;
    }
}
<?php

class CreateTestModel extends ResourceHandler
{
    //necessary resources for every class
    private $resource;
    private $db;

    private function getResource()
    {
        $this->resource = $_SESSION['reg']->registry['resource'];
    }

    private function getDb()
    {
        $this->db = $_SESSION['reg']->registry['database'];
    }

}